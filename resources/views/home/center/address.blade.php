@extends('home.center.layouts.master')
@push('js')
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery.reveal.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/chengs/jquery.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/chengs/bootstrap.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/chengs/city-picker.data.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/chengs/city-picker.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/chengs/main.js"></script>
@endpush
@section('content')
    <div class="mod_main">
        <div class="jib_xinx_kuang">
            <div class="shand_piaot">收货地址</div>
            <div class="shouh_diz_beij">
                @foreach($addresses as $k=>$address)
                    <div class="shouh_diz_kuang shouh_diz_kuang_mor">
                        <div class="item">
                            <span class="labal">收件人：</span>
                            <p>{{$address['name']}}</p>
                        </div>
                        <div class="item">
                            <span class="labal">所在地区：</span>
                            <p>{{$address['area']}}</p>
                        </div>
                        <div class="item">
                            <span class="labal">地址：</span>
                            <p>{{$address['address']}}</p>
                        </div>
                        <div class="item">
                            <span class="labal">手机：</span>
                            <p>{{$address['phone']}}</p>
                        </div>
                        <div class="bianj_yv_shanc">
                            @if($address['is_default'])
                                <a href="#" class="mor_color">当前默认地址</a>
                            @else
                                <a href="{{route('home.setDefault',$address)}}">设为默认</a>
                            @endif
                            <a href="javascript:;" data-reveal-id="myModal_{{$address['id']}}">编辑</a>
                            <a href="javascript:;" onclick="del(this)">删除</a>
                            <form action="{{route('home.address.destroy',$address)}}" method="post">
                                @csrf
                                @method('DELETE')
                            </form>
                        </div>
                    </div>
                    {{--编辑地址表单部分--}}
                    <form action="{{route('home.address.update',$address)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="reveal-modal" id="myModal_{{$address['id']}}" >
                            <div class="xint_biaot">
                                <h3>编辑收货地址</h3>
                            </div>
                            <div class="shouj_diz_b">
                                <div class="biaod_1">
                                    <p><em>*</em>联系人：</p>
                                    <input value="{{$address['name']}}" type="text" class="text" name="name">
                                </div>
                                <div class="biaod_1">
                                    <p><em>*</em>收货地址：</p>
                                    <div class="xinz_diz_cs">
                                        <div class="docs-methods">
                                            <form class="form-inline">
                                                <div id="distpicker">
                                                    <div class="form-group">
                                                        <div style="position: relative;">
                                                            <input name="area" id="city-picker3" class="form-control" readonly
                                                                   type="text"
                                                                   value="{{$address['area']}}" data-toggle="city-picker">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="biaod_1">
                                    <p><em>*</em>详细地址：</p>
                                    <input value="{{$address['address']}}" name="address" type="text" class="text text1">
                                </div>
                                <div class="biaod_1 biaod_2">
                                    <div class="liangp_e">
                                        <p><em>*</em>手机号码：</p>
                                        <input value="{{$address['phone']}}" name="phone" type="text" class="text">
                                    </div>
                                </div>
                                <div class="biaod_1 form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="is_default" id=""
                                               value="1" {{$address['is_default']==1?'checked':''}}>
                                        设置为默认地址
                                    </label>
                                </div>
                                <div class="biaod_1">
                                    <button type="submit" class="btn-primary btn">保存</button>
                                </div>
                            </div>
                            <a class="close-reveal-modal">&#215;</a>
                        </div>
                    </form>
                    {{--编辑地址表单部分结束--}}
                @endforeach
                <div class="xinz_shouh_dz_ann"><a href="#" data-reveal-id="myModal">新增收货地址</a></div>
            </div>
        </div>

    </div>
    {{--模态框js代码--}}
    <script>
        function del(obj) {
            swal({
                title: '确定删除吗？',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: 'red',
            }).then(function (res) {
                // 判断获取的回调是否为真,如果为真,执行删除动作
                if (res) {
                    $(obj).next().submit();
                }
            })
        }
    </script>
    {{--模态框js代码结束--}}

    <!--右边内容结束-->

    <!--新增地址表单弹框-->
    <form action="{{route('home.address.store')}}" method="post">
        @csrf
        <div id="myModal" class="reveal-modal">
            <div class="xint_biaot">
                <h3>新添收货地址</h3>
            </div>
            <div class="shouj_diz_b">
                <div class="biaod_1">
                    <p><em>*</em>联系人：</p>
                    <input type="text" class="text" name="name">
                </div>
                <div class="biaod_1">
                    <p><em>*</em>收货地址：</p>
                    <div class="xinz_diz_cs">
                        <div class="docs-methods">
                            <form class="form-inline">
                                <div id="distpicker">
                                    <div class="form-group">
                                        <div style="position: relative;">
                                            <input name="area" id="city-picker3" class="form-control" readonly
                                                   type="text"
                                                   value="北京市/北京市/朝阳区" data-toggle="city-picker">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="biaod_1">
                    <p><em>*</em>详细地址：</p>
                    <input name="address" type="text" class="text text1">
                </div>
                <div class="biaod_1 biaod_2">
                    <div class="liangp_e">
                        <p><em>*</em>手机号码：</p>
                        <input name="phone" type="text" class="text">
                    </div>
                </div>
                <div class="biaod_1 form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="is_default" id="" value="1">
                        设置为默认地址
                    </label>
                </div>
                <div class="biaod_1">
                    <button type="submit" class="diz_baoc">保存</button>
                </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
        </div>
    </form>

@endsection
