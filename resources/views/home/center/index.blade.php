@extends('home.center.layouts.master')
@section('content')
    <!--右边内容-->
    <div class="mod_main" style="min-height: 0">
        <div class="mt">
            <h3>我的订单(最近3个)</h3>
            <div class="extra-r" style="margin-right: 40px;"><a href="{{route('home.center','order')}}">查看全部订单</a></div>
        </div>
        <div class="tb_order">
            <table width="100%">
                @foreach($orders as $k=>$order)
                    @if($k<3)
                        <tbody class="fore0">
                        @foreach($order->order_detail as $k=>$order_detail)
                            @if($k<1)
                                <tr>
                                    <td>
                                        <div class="img-list"><a href="javascript:;" target="_blank"><img
                                                    src="{{$order_detail->goods->pics[0]}}"
                                                    title="{{$order_detail->goods->title}}"></a></div>
                                    </td>
                                    <td>
                                        <div class="u-name">{{$order->order_address->name}}</div>
                                    </td>
                                    <td>￥{{$order_detail->goods->price}}<br>微信支付</td>
                                    <td><span class="ftx-03">{{$order->created_at}}</span></td>
                                    <td><span class="ftx-03">{{$order->status}}</span></td>
                                    <td class="order-doi"><a target="_blank"
                                                             href="{{route('home.center','order')}}">查看</a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    @endif
                @endforeach
            </table>
        </div>
    </div>
    <div class="mod_main mod_main1">
        <div class="mt">
            <h3>我收藏的商品</h3>
            <div class="extra-r"><a href="javascript:;">查看更多</a></div>
        </div>
        <div class="follow">
            <ul>
                <li><a class="follow_tup_kuang" href="javascript:;"><img
                            src="{{asset('org/home/images')}}/lieb_tupi2.jpg"></a>
                    <p>暂无售价</p></li>
                <li><a class="follow_tup_kuang" href="javascript:;"><img
                            src="{{asset('org/home/images')}}/lieb_tupi1.jpg"></a>
                    <p>￥25.00</p></li>
                <li><a class="follow_tup_kuang" href="javascript:;"><img
                            src="{{asset('org/home/images')}}/lieb_tupi3.jpg"></a>
                    <p>暂无售价</p></li>
                <li><a class="follow_tup_kuang" href="javascript:;"><img
                            src="{{asset('org/home/images')}}/lieb_tupi2.jpg"></a>
                    <p>暂无售价</p></li>
                <li><a class="follow_tup_kuang" href="javascript:;"><img
                            src="{{asset('org/home/images')}}/lieb_tupi3.jpg"></a>
                    <p>暂无售价</p></li>
                <li><a class="follow_tup_kuang" href="javascript:;"><img
                            src="{{asset('org/home/images')}}/lieb_tupi1.jpg"></a>
                    <p>暂无售价</p></li>
            </ul>
        </div>
    </div>
    <div class="mod_main mod_main1 mod_main2">
        <div class="mt">
            <h3>我收藏的商品</h3>
            <div class="extra-r"><a href="javascript:;">查看更多</a></div>
        </div>
        <div class="follow">
            <ul>
                <li>
                    <a class="follow_tup_kuang" href="javascript:;"><img
                            src="{{asset('org/home/images')}}/xiangqtu_1.jpg"></a>
                    <p><a href="javascript:;">MI手机 小米Note3 全网通版 6GB+128GB 亮蓝 移动联通电信4G手机 双卡双待<span>(已有100+人评价)</span></a>
                    <p class="p_color_1">￥52.00</p></p>
                </li>
                <li>
                    <a class="follow_tup_kuang" href="javascript:;"><img
                            src="{{asset('org/home/images')}}/yic_003.jpg"></a>
                    <p><a href="javascript:;">MI手机 小米Note3 全网通版 6GB+128GB 亮蓝 移动联通电信4G手机 双卡双待<span>(已有100+人评价)</span></a>
                    <p class="p_color_1">￥52.00</p></p>
                </li>
                <li>
                    <a class="follow_tup_kuang" href="javascript:;"><img
                            src="{{asset('org/home/images')}}/shangq_1.jpg"></a>
                    <p><a href="javascript:;">MI手机 小米Note3 全网通版 6GB+128GB 亮蓝 移动联通电信4G手机 双卡双待<span>(已有100+人评价)</span></a>
                    <p class="p_color_1">￥52.00</p></p>
                </li>
                <li>
                    <a class="follow_tup_kuang" href="javascript:;"><img
                            src="{{asset('org/home/images')}}/shangq_3.jpg"></a>
                    <p><a href="javascript:;">MI手机 小米Note3 全网通版 6GB+128GB 亮蓝 移动联通电信4G手机 双卡双待<span>(已有100+人评价)</span></a>
                    <p class="p_color_1">￥52.00</p></p>
                </li>
                <li>
                    <a class="follow_tup_kuang" href="javascript:;"><img
                            src="{{asset('org/home/images')}}/rem_shangp.jpg"></a>
                    <p><a href="javascript:;">MI手机 小米Note3 全网通版 6GB+128GB 亮蓝 移动联通电信4G手机 双卡双待<span>(已有100+人评价)</span></a>
                    <p class="p_color_1">￥52.00</p></p>
                </li>
            </ul>
        </div>
    </div>
@endsection
