<!DOCTYPE html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>个人中心首页</title>
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/index.css">
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/ziy.css">
    <script type="text/javascript" src="{{asset('org/home/js/sweetalert.min.js')}}"></script>
    @stack('css')
    @stack('js')
</head>
<body>
@include('layouts._home_message')
<!--头部-->
<div id="header">
    <div class="header-box">
        <h3 class="huany">欢迎您的到来！</h3>

        <ul class="header-right">
            <li class="denglu dengl_hou">
                <div>
                    <a class="red" href="{{route('home.center','basic')}}"
                       style="padding-right: 0px;">Hi~{{auth()->user()->nickName}}</a>
                    <i class="ci-leftll">
                        <s class="jt">◇</s>
                    </i>
                </div>
                <div class="dengl_hou_xial_k">
                    <div class="zuid_xiao_toux">
                        <a href="javascript:;"><img src="{{auth()->user()->icon??''}}"></a>
                    </div>
                    <div class="toub_zil_daoh">
                        <a href="{{route('home.center','order')}}">待处理订单</a>
                        <a href="javascript:;">我的收藏</a>
                        <a href="{{route('home.center','basic')}}">个人资料</a>
                    </div>
                </div>
            </li>
            <li class="shu"></li>
            <li class="shouji bj">
                <a class="" href="{{route('home.index')}}">退出个人中心</a>
                <i class="ci-right ">
                    <s class="jt">◇</s>
                </i>
            </li>
            <li class="shu"></li>
            <li class="denglu"><a class="ing_ps" href="javascript:;">我的收藏</a></li>
            <li class="shu"></li>
        </ul>
    </div>
</div>
<div class="hongs_beij">
    <div class="beij_center">
        <div class="wode_tongc_logo">
            <a class="tongc_logo" href="javascript:;"></a>

        </div>
        <div class="navitems">
            <ul>
                <li><a href="{{route('home.center','index')}}"><span>首页</span> </a></li>
                <li>
                    <div class="zhangh_dl">
                        <div class="zhangh_dt"><span>账号设置</span><i>◇</i></div>
                        <div class="zhangh_dd">
                            <a href="{{route('home.center','basic')}}">个人信息</a>
                            <a href="{{route('home.center','password')}}">修改密码</a>
                            <a href="{{route('home.center','address')}}">收货地址</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="nav_r">
            <div id="search_2014">
                <div class="form">
                    <input type="text" class="gerzx_text">
                    <button class="button1"></button>
                </div>
            </div>
            <div id="settleup_2014">
                <div class="cw_icon">
                    <a href=""><span>购物车<em>{{count($carts)}}</em>件</span></a>
                    <i class="ci-right ">
                        <s class="jt">◇</s>
                    </i>
                </div>
                <div class="dorpdown-layer">
                    <div class="smt"><h4 class="fl">最新加入的商品</h4></div>
                    <ul>
                        @foreach($carts as $cart)
                            <li class="meiyou">
                                <div class="gouwc_tup">
                                    <a href="{{route('home.center','index')}}"><img src="{{$cart->goods->pics[0]}}"></a>
                                </div>
                                <div class="gouwc_biaot">
                                    <a href="javascript:;">{{$cart->goods->title}}</a>
                                </div>
                                <div class="gouwc_shanc">
                                    <span>￥{{$cart->goods->price}}</span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--主题部分-->
<div class="wod_tongc_zhongx">
    <div class="beij_center">
        <div class="myGomeWeb">
            <!--侧边导航-->
            <div class="tod_tongc_zuoc">
                <div class="zuoc_toux">
                    <div class="toux_kuang">
                        <div class="userImage">
                            <div class="myGome_userPhoto">
                                <img src="{{auth()->user()->icon??''}}">
                                <a class="edit_photo_bitton" href="javascript:;" target="_blank">编辑</a>
                            </div>
                        </div>
                        <div class="user_name_Level">
                            <p class="user_name" title="山的那边是海">{{auth()->user()->nickName}}</p>
                            <p class="userLevel">会员：<span class="levelId icon_plus_nickname"></span></p>
                        </div>
                    </div>
                    <div class="userInfo_bar">
                        <span>资料完成度</span>
                        <span class="userInfo_process_bar"><em class="active_bar" style="width: 40px;"> 20%</em></span>
                        <a href="javascript:;" target="_blank">完善</a>
                    </div>
                    <div class="myGome_accountSecurity">
                        <span class="fl_ee" style="margin-top:2px;">账户安全 <em
                                class="myGome_account_level"> 低</em> </span>
                        <div class="verifiBox fl_ee">
                            <div class="shab_1">
                                <span class="myGome_mobile" val="mobile"> <em class=" myGome_onActive "></em> </span>
                                <p class="myGome_verifiPop"><span>您已绑定手机：</span> <span>182****0710</span> <a
                                        href="javascript:;"
                                        target="_blank">管理</a>
                                </p>
                            </div>
                            <div class="shab_1">
                                <span class="myGome_email" val="email"> <em class=""></em> </span>
                                <p class="myGome_verifiPop"><span>您还未绑定邮箱 </span><a href="javascript:;" target="_blank">立即绑定</a>
                                </p>
                            </div>
                        </div>
                        <a class="fl_ee" href="javascript:;" target="_blank" style="margin-top:2px;">提升</a>
                    </div>
                    <div class="user_counts">
                        <ul>
                            <li>
                                <div class="count_item">
                                    <a href="javascript:;">
                                        <i class="count_icon count_icon01"></i> 待付款
                                        <em id="waitPay">2</em>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="count_item">
                                    <a href="javascript:;">
                                        <i class="count_icon count_icon02"></i> 待收货
                                        <em id="waitPay">4</em>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="count_item">
                                    <a href="javascript:;">
                                        <i class="count_icon count_icon03"></i> 待提货
                                        <em id="waitPay">0</em>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="count_item">
                                    <a href="javascript:;">
                                        <i class="count_icon count_icon04"></i> 待评价
                                        <em id="waitPay">8</em>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="wod_tongc_daoh_lieb">
                    <div class="diy_top">
                        <ul>
                            <h3>订单中心</h3>
                            <li><a href="{{route('home.center','order')}}">我的订单</a></li>
                        </ul>
                        <ul>
                            <h3>管理中心</h3>
                            <li><a href="wod_shouc.html">我的收藏</a></li>
                        </ul>
                    </div>
                    <div class="diy_top">
                        <ul>
                            <h3>账户设置</h3>
                            <li><a href="{{route('home.center','basic')}}">基本资料</a></li>
                            <li><a href="{{route('home.center','password')}}">修改密码</a></li>
                            <li><a href="{{route('home.center','address')}}">收货地址</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            @yield('content')
        </div>
    </div>
</div>
<!--底部-->
<div class="dib_beij dib_beij_ger_zhongx">
    <div class="dib_jvz_beij">
        <div class="shangp_baoz">
            <p>本地购物&nbsp;&nbsp;看得见的商品</p>
            <p class="beng1">正品行货&nbsp;&nbsp;购物无忧</p>
            <p class="beng2">低价实惠&nbsp;&nbsp;帮你省钱</p>
            <p class="beng3">本地直发&nbsp;&nbsp;专业配送</p>
        </div>
        <div class="zhongj_youx">
            <div class="lieb_daoh">
                <h4>物流配送</h4>
                <ul>
                    <li><a href="javascript:;">配送查询</a></li>
                    <li><a href="javascript:;">配送服务</a></li>
                    <li><a href="javascript:;">配送费用</a></li>
                    <li><a href="javascript:;">配送时效</a></li>
                    <li><a href="javascript:;">签收与验货</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>支付与账户</h4>
                <ul>
                    <li><a href="javascript:;">货到付款</a></li>
                    <li><a href="javascript:;">在线支付</a></li>
                    <li><a href="javascript:;">门店支付</a></li>
                    <li><a href="javascript:;">账户安全</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>购物帮助</h4>
                <ul>
                    <li><a href="javascript:;">购物保障</a></li>
                    <li><a href="javascript:;">购物流程</a></li>
                    <li><a href="javascript:;">焦点问题</a></li>
                    <li><a href="javascript:;">联系我们</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>售后服务</h4>
                <ul>
                    <li><a href="javascript:;">退换货服务</a></li>
                    <li><a href="javascript:;">退款说明</a></li>
                    <li><a href="javascript:;">专业维修</a></li>
                    <li><a href="javascript:;">延保服务</a></li>
                    <li><a href="javascript:;">家电回收</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <div class="kef_dianh">
                    <p>客服电话</p><span>15110853679</span>
                </div>
                <div class="kef_dianh kef_dianh_youx">
                    <p>意见收集邮箱</p>
                    <p>237618121@qq.com</p>
                </div>
            </div>
            <div class="lieb_daoh lieb_daoh_you">
                <div class="erw_ma_beij">
                    <div class="erw_m">
                        <h1><img src="{{asset('org/home/images')}}/mb_wangid.png"></h1>
                        <span>扫码下载通城客户端</span>
                    </div>
                    <div class="erw_m">
                        <h1><img src="{{asset('org/home/images')}}/mb_wangid.png"></h1>
                        <span>扫码下载通城客户端</span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

</body>
</html>
