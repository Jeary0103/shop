@extends('home.center.layouts.master')
@section('content')
    <div class="mod_main" style="padding-top: 50px">
        <form method="post" action="{{route('home.changePassword')}}">
            @csrf
            @method('PUT')
            <div class="reg-items">
              	<span class="reg-label">
                	<label for="J_Name">设置新密码：</label>
              	</span>
                <input class="i-text" type="password" name="newPassword">
            </div>
            <div class="reg-items">
              	<span class="reg-label">
                	<label for="J_Name">确认密码：</label>
              	</span>
                <input class="i-text" type="password" name="newPassword_confirmation">
            </div>
            <div class="reg-items">
              	<span class="reg-label">
                	<label for="J_Name"> </label>
              	</span>
                <input class="reg-btn" value="保存修改" type="submit">
            </div>
        </form>
    </div>
@endsection
