@extends('home.center.layouts.master')
@push('js')
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery-1.11.1.min.js"></script>
    @include('layouts._hdjs')
@endpush
@section('content')
    <div class="mod_main">
        <div class="jib_xinx_kuang">
            <div class="shand_piaot">我的订单</div>
            <table class="order-tb order-tb_1">
                <colgroup>
                    <col class="number-col">
                    <col class="consignee-col">
                    <col class="amount-col">
                    <col class="operate-col">
                    <col class="dis_col">
                </colgroup>
                <thead>
                {{--头部不需要循环--}}
                <tr>
                    <th>订单详情</th>
                    <th>金额</th>
                    <th>订单状态</th>
                    <th>收货人</th>
                    <th>操作</th>
                </tr>
                {{--头部不需要循环--}}
                </thead>
                @foreach($orders as $order)
                    <tbody>
                    <tr class="sep-row">
                        <td colspan="4"></td>
                    </tr>
                    <tr class="tr-th">
                        <td colspan="5">
                            <span class="gap"></span>
                            <span class="dealtime span_30" title="2015-1-19 10:30:42">{{$order->created_at}}</span>
                            <span class="number span_30">订单号：<a href="#"
                                                                target="_blank">{{$order->order_bianhao}}</a></span>
                            <span class="wod_sc_delete_beij span_30"><a href="javascript:;" order_id_a="{{$order->id}}"
                                                                        class="wod_dingd_delete"></a></span>
                        </td>
                    </tr>
                    <tr class="tr-bd">
                        <td>
                            @foreach($order->order_detail as $v)
                                <div class="goods-item" style="margin-top: 14px;">
                                    <div class="p-img">
                                        <a target="_blank" href="{{route('home.content',$v->goods->id)}}">
                                            <img src="{{$v->goods->pics[0]}}" alt="">
                                        </a>
                                    </div>
                                    <div class="p-msg">
                                        <div class="p-name">
                                            <a target="_blank" href="{{route('home.content',$v->goods->id)}}">{{$v->goods->title}}</a>
                                        </div>
                                    </div>
                                    <div class="goods-number">x{{$v->num}}</div>
                                </div>
                            @endforeach
                        </td>

                        <td rowspan="2">
                            <div class="zhif_jine">
                                <p>总额￥{{$order->total_price}}</p>
                                <span>微信支付</span>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div class="operate">
                                <p class="yiwanc_hui">{{$order->status}}</p>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div class="txt_ren">
                                <p class="ren_tub"></p>
                                <span>{{$order->order_address->name}}</span>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div class="operate">
                                <a href="#" target="_blank" class="btn-def">再次购买</a>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                @endforeach
            </table>
            <script>
                $(function () {
                    $('.wod_sc_delete_beij').click(function () {
                        let order_id = $(this).children().attr('order_id_a');
                        $.ajax({
                            type: "get",
                            url: "/delOrder/" + order_id,
                            dataType: "json",
                            success: function (res) {
                                if(res.code){
                                    swal({
                                        text:'删除成功！',
                                        button:false,
                                        icon:'success'
                                    });
                                    location.reload();
                                }
                            }
                        });
                    });
                });
            </script>
            <div class="gerzx_fany">
                <a href="#" class="shangxy">上一页</a>
                <a href="#">1</a>
                <a href="#" class="shangxy">上一页</a>
            </div>
        </div>
    </div>
@endsection
