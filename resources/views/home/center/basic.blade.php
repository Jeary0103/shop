@extends('home.center.layouts.master')
@push('css')
    <link rel="stylesheet" href="{{asset('org/layui')}}/css/layui.css">
@endpush

@section('content')
    <div class="mod_main">
        <div class="jib_xinx_kuang">
            <div class="wt">
                <ul>
                    <li class="dangq_hongx"><a href="{{route('home.center','basic')}}">个人信息</a></li>
                </ul>
            </div>

            {{--表单提交--}}
            <div class="wd">
                <form action="{{route('home.gerenXinxi')}}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="user_set">
                        <div class="item_meic">
                            <span class="label_meic"><em>*</em> 昵称：</span>
                            <div class="fl_e">
                                <input type="text" class="itxt_succ itxt_succ_url" maxlength="20" id="nickName"
                                       name="nickName" value="{{$user['nickName']}}">
                            </div>
                        </div>
                        <div class="item_meic">
                            <span class="label_meic"><em>*</em> 姓名：</span>
                            <div class="fl_e">
                                <input type="text" value="{{$user['name']}}" name="name" class="user_address">
                            </div>
                        </div>
                        <div class="item_meic">
                            <span class="label_meic"><em>*</em> 邮箱：</span>
                            <div class="fl_e">
                                <input type="text" value="{{$user['email']}}" name="email" disabled
                                       class="user_address">
                                <span>&nbsp;&nbsp;注：邮箱作为登录名，不能随便修改</span>
                            </div>
                        </div>
                        <div class="item_meic">
                            <span class="label_meic">性别：</span>
                            <div class="fl_e">
                                <input type="radio" name="sex" class="jdradio"
                                       {{$user['sex']=='男'?'checked':''}} value="男" id="11">
                                <label class="mr10" for="11">男</label>
                                <input type="radio" name="sex" class="jdradio"
                                       {{$user['sex']=='女'?'checked':''}}  value="女" id="00">
                                <label class="mr10" for="00">女</label>
                            </div>
                        </div>
                        <form class="layui-form" action="">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="padding-right: 5px;">出生日期：</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="age" id="date" lay-verify="date" placeholder="2008-8-8"
                                           value="{{$user['age']??old('name')}}" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                        </form>
                        <br/>

                        {{--头像上传--}}
                        <div class="layui-upload">
                            <button type="button" class="layui-btn" id="test1" style="background: #DF3033;
float:left; margin-top:40px; margin-right: 20px;">上传头像</button>
                            <input type="text" hidden name="icon" id="icon" >
                            <div class="layui-upload-list myGome_userPhoto" style="display: inline-block">
                                <img style="width: 92px;height: 92px;" src="{{$user['icon']}}" class="layui-upload-img" id="demo1">
                                <p id="demoText"></p>
                            </div>
                        </div>

                        {{--保存按钮--}}
                        <div class="item_meic" style="margin-top: 150px; margin-left: 18px;">
                            <span class="label_meic"> </span>
                            <div class="fl_e">
                                <input type="submit" value="保存" class="savebtn">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        // layui出生日期代码片段
        layui.use(['form', 'layedit', 'laydate'], function () {
            var form = layui.form
                , layer = layui.layer
                , layedit = layui.layedit
                , laydate = layui.laydate;

            //日期
            laydate.render({
                elem: '#date'
            });
            laydate.render({
                elem: '#date1'
            });
        });
    </script>
@endsection
@push('js')
    {{--<script>--}}
    {{--//JavaScript代码区域--}}
    {{--layui.use('element', function () {--}}
    {{--var element = layui.element;--}}
    {{--});--}}
    {{--</script>--}}
    {{--引入layui的js文件--}}
    <script type="text/javascript" src="{{asset('org/layui')}}/layui.js"></script>
    <script>
        layui.use('upload', function(){
            var $ = layui.jquery
                ,upload = layui.upload;

            //普通图片上传
            var uploadInst = upload.render({
                elem: '#test1'
                ,url: '{{route('common.uploader.layuiUpload')}}'
                ,data:{
                    _token:'{{csrf_token()}}'
                }
                ,before: function(obj){
                    //预读本地文件示例，不支持ie8
                    // obj.preview(function(index, file, result){
                    //     $('#demo1').attr('src', result); //图片链接（base64）
                    // });
                }
                ,done: function(res){
                    //console.log(res)
                    //如果上传失败
                    if(res.code > 0){
                        return layer.msg('上传失败');
                    }
                    //上传成功
                    $('#demo1').attr('src', res.data.src);
                    $('#icon').val(res.data.src);
                }
                ,error: function(){
                    //演示失败状态，并实现重传
                    var demoText = $('#demoText');
                    demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                    demoText.find('.demo-reload').on('click', function(){
                        uploadInst.upload();
                    });
                }
            });
        });
    </script>
@endpush

