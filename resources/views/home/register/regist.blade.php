<!DOCTYPE html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>个人注册</title>
    <link href="https://cdn.bootcss.com/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/index.css">
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/ziy.css">
    @include('layouts._hdjs')
</head>
<body>
@include('layouts._message')
<!--dengl-->
<div class="yiny">
    <div class="beij_center">
        <div class="dengl_logo">
            <img src="{{asset('org/home/images')}}/logo_1.png">
            <h1>欢迎注册</h1>
        </div>
    </div>
</div>
<div class="beij_center">
    <div class="ger_zhuc_beij">
        <div class="ger_zhuc_biaot">
            <ul>
                <li><a href="shenq_ruz.html">申请入驻</a></li>
                <p>我已经注册，现在就<a class="ftx-05 ml10" href="{{route('home.login')}}">登录</a></p>
            </ul>
        </div>
        <form action="{{route('home.regist')}}" method="post">
            @csrf
            <div class="zhuc_biaod">
                <div class="reg-items">
              	<span class="reg-label">
                	<label for="J_Name">昵称：</label>
              	</span>
                    <input name="name" class="i-text" type="text" value="">
                </div>
                <div class="reg-items">
              	<span class="reg-label">
                	<label for="J_Name">邮箱：</label>
              	</span>
                    <input class="i-text" type="text" name="email" value="" placeholder="">
                </div>
                <div class="reg-items">
              	<span class="reg-label">
                	<label for="J_Name">设置密码：</label>
              	</span>
                    <input class="i-text" type="password" name="password" value="">
                </div>
                <div class="reg-items">
              	<span class="reg-label">
                	<label for="J_Name">确认密码：</label>
              	</span>
                    <input class="i-text" type="password" name="password_confirmation" value="">
                </div>
                <div class="reg-items">
              	<span class="reg-label">
                	<label for="J_Name">验证码：</label>
              	</span>
                    <input class="i-text i-short" type="text" name="code">
                    <div class="check " style="position:relative;left:0">
                        <a class="check-phone" id="bt" style="padding:11px 14px 14px 14px;*line-height:60px;background: #76ABBD; color:#fff;border-radius: 3px;">获取验证码</a>
                        <span class="check-phone disable" style="display: none;"><em>60</em>秒后重新获取</span>
                        <a class="check-phone" style="display: none;padding:11px 10px 14px 10px">重新获取验证码</a>
                    </div>
                    <div class="msg-box">
                        <div class="msg-weak err-tips" style="display:none;">
                            <div>请输入短信验证码</div>
                        </div>
                    </div>
                </div>
                <div class="reg-items">
              	<span class="reg-label">
                	<label for="J_Name"> </label>
              	</span>
                    <div class="dag_biaod">
                        <input type="checkbox" value="english">
                        阅读并同意
                        <a href="#" class="ftx-05 ml10">《用户注册协议》</a>
                        <a href="#" class="ftx-05 ml10">《隐私协议》</a>
                    </div>
                </div>
                <div class="reg-items">
              	<span class="reg-label">
                	<label for="J_Name"> </label>
              	</span>
                    <input class="reg-btn" value="立即注册" type="submit">
                </div>
            </div>
        </form>
        <script>
            require(['hdjs','bootstrap'], function (hdjs) {
                let option = {
                    //按钮
                    el: '#bt',  // 1. 登录按钮的id值
                    //后台链接，其实就是处理逻辑的一个方法地址，通过路由可以找到该方法，要新建控制器
                    url: "{{route('common.code.send')}}", // 2. 通过路由，进入到验证处理的控制器方法
                    //验证码等待发送时间
                    timeout: 5,
                    //表单，手机号或邮箱的INPUT表单
                    input: '[name="email"]' // 3. 是要验证的表单内容值，比如，邮箱，给邮箱发送验证码
                };
                hdjs.validCode(option);
            })
        </script>
        <div class="xiaogg">
            <img src="{{asset('org/home/images')}}/cdsgfd.jpg">
        </div>
    </div>
</div>

</body>
</html>
