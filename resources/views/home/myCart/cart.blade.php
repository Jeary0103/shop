<!DOCTYPE html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <meta charset="utf-8">
    <title>购物车</title>

    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/index.css">
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/ziy.css">
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery1.42.min.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery-1.11.1.min.js"></script>
    {{--<script type="text/javascript" src="{{asset('org/home/js')}}/jquery.SuperSlide.2.1.1.source.js"></script>--}}
    <script type="text/javascript" src="{{asset('org/home/js')}}/select.js"></script>
    @include('layouts._hdjs')
</head>
<body>
<!--头部-->

<!--头部-->
<div id="header">
    <div class="header-box">
        <h3 class="huany">欢迎您的到来！</h3>
        <ul class="header-right">
            <li class="denglu dengl_hou">
                <div>
                    <a class="red" href="{{route('home.center','basic')}}">{{auth()->user()->name}}</a>
                    <i class="icon_plus_nickname"></i>
                    <i class="ci-leftll">
                        <s class="jt">◇</s>
                    </i>
                </div>
                <div class="dengl_hou_xial_k">
                    <div class="zuid_xiao_toux">
                        <a href="{{route('home.center','basic')}}"><img src="{{auth()->user()->icon??''}}"></a>
                    </div>
                    <div class="huiy_dengj">
                        <a class="tuic_" href="{{route('home.logout')}}">退出</a>
                    </div>
                    <div class="toub_zil_daoh">
                        <a href="{{route('home.center','order')}}">待处理订单</a>
                        <a href="#">我的收藏</a>
                        <a href="{{route('home.center','basic')}}">个人资料</a>
                    </div>
                </div>
            </li>
            <li class="shu"></li>
            <li class="denglu"><a class="ing_ps" href="#">我的收藏</a></li>
        </ul>
    </div>
</div>
{{--头部结束--}}

<!---->
<div class="yiny yiny_gouwc">
    <div class="beij_center">
        <div class="dengl_logo">
            <img src="{{asset('org/home/images')}}/logo_1.png">
            <h1>购物车</h1>
        </div>
    </div>
</div>
<div class="container">
    <div class="cart-login-tip fl" id="idnotlogin" act-not-login-info="" style="display: none;">
        您还没有登录！登录后购物车的商品将保存到您账号中。
        <a class="cart-login-btn" href="{{route('home.login')}}">
            立即登录
        </a>
    </div>
</div>
<style>
    [v-cloak] {
        display: none;
    }
</style>
{{--vue 代码块开始了--}}
<div id="app" v-cloak="">
    {{--上部--}}
    <div v-if="carts.length" class="beij_center">
        <div class="cart-main-header clearfix">
            <div class="cart-col-1">
                <input type="checkbox" class="jdcheckbox" @change="changeChecked()" v-model="allChecked">
            </div>
            <div class="cart-col-2">全选</div>
            <div class="cart-col-3">商品信息</div>
            <div class="cart-col-4">
                <div class="cart-good-real-price">单价</div>
            </div>
            <div class="cart-col-5" style="padding-left: 20px;">数量</div>
            <div class="cart-col-6">
                <div class="cart-good-amount" style="padding-left: 12px;">小计</div>
            </div>
            <div class="cart-col-7" style="padding-left: 5px;">操作</div>
        </div>
    </div>
    <div v-else
         style="text-align: center;margin: 0 auto; width:1198px; border:1px solid #ccc;line-height: 100px; height:100px;">
        <h1>亲，购物车中没有商品呀！<a href="/" style="font-size: 20px;"> &nbsp;&nbsp;先去挑选几件吧...</a></h1>

    </div>
    {{--内容部分--}}

    <div class="container">
        <div class="cart-shop-goods" v-for="(v,k) in carts">
            <div class="cart-shop-good">
                <div class="cart-col-1">
                    <input type="checkbox" class="jdcheckbox" v-model="v.checked">
                </div>
                <div class="cart-col-2" style="height: 42px;">
                    <a href="" target="_blank" class="g-img"><img :src="v.goods.pics[0]" alt=""></a>
                </div>
                <div class="fl houj_c">
                    <div class="cart-dfsg">
                        <div class="cart-good-name"><a href="#" target="_blank">@{{ v.goods.title }}</a></div>
                    </div>
                    <div class="cart-good-pro"></div>
                    <div class="cart-col-4">
                        <div class="cart-good-real-price ">¥&nbsp;@{{ v.goods.price+v.product.add_price }}</div>
                        <!-- 单价 -->
                        <div class="red"></div>
                    </div>
                    <div class="cart-col-5">
                        <div class="gui-count cart-count">
                            <a href="#" gui-count-sub="" @click.prevent="minus(v)"
                               class="gui-count-btn gui-count-sub gui-count-disabled" style="cursor: pointer">-</a>
                            <a href="#" @click.prevent="plus(v)" gui-count-add=""
                               class="gui-count-btn gui-count-add">+</a>
                            <div class="gui-count-input"><input dytest="" class="" type="text" :value="v.num"></div>
                        </div>
                    </div>
                    <div class="cart-col-6 ">
                        <div class="cart-good-amount">¥@{{v.xiaoji}}</div><!-- 小计 --></div>
                </div>
                <div class="cart-col-7" style="text-align: center">
                    <div class="cart-good-fun delfixed">
                        <a href="" @click.prevent="del(v,k)">删除</a>
                    </div>
                    <div class="cart-good-fun" style="margin-top: 5px;">
                        <a href="#">移入收藏夹</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--下部--}}
    <div class="jies_beij" v-if="carts.length">
        <div class="beij_center over_dis">
            <div class="cart-col-1 cart_jies">
                <input type="checkbox" class="jdcheckbox" @change="changeChecked()" v-model="allChecked">
            </div>
            <div class="qianm_shanc_yvf">
                <span>全选</span>
                <a href="#">删除</a>
            </div>
            <div class="jies_ann_bei">
                <p>已选 <em>@{{ select_num }}</em> 件商品 总计（不含运费）：<span>￥@{{ total }}</span></p>
                <a href="javascript:;" @click="jiesuan()" class="order_btn">去结算</a>
            </div>
        </div>
    </div>
</div>
<script>
    require(['hdjs', 'vue', 'axios'], function (hdjs, vue, axios) {
        new vue({
            el: "#app",
            data: {
                carts:{!! json_encode($carts) !!},
                allChecked: false, // 定义全选按钮的状态值，默认值为false
            },
            computed: { // 自动计算属性可以用来计算总价，模板上可以直接使用函数名来输出计算结果
                total() {
                    var zongji = 0;
                    this.carts.forEach((v) => {
                        // 判断如果v里面的选中状态为真,才获取它的小计,来计算总价
                        if (v.checked) {
                            zongji += v.xiaoji;
                        }
                    })
                    return zongji;
                },
                select_num() {
                    var number = 0;
                    this.carts.forEach((v) => {
                        if (v.checked) {
                            number++;
                        }
                    });
                    return number;
                }

            },
            methods: {
                // 去结算的方法
                jiesuan() {
                    var zongjia = 0;
                    this.carts.forEach((v) => {
                        if (v.checked) {
                            zongjia += v.xiaoji
                        }
                    })
                    if (zongjia == 0) {
                        hdjs.swal({
                            text: '请至少选择一个货品',
                            button: false,
                            icon: 'error'
                        });
                        return false;
                    }
                    // 获取购物车被选中商品的购物车id
                    let ids = '';
                    this.carts.forEach((v) => {
                        if (v.checked) {
                            ids += v.id + ',';
                        }
                    });
                    // 当选择好商品之后，下一步就是去结算页面，即订单展示页面，需要把当前的购物车ID带过去
                    location.href = '{{route('home.order_list')}}' + '?ids=' + ids;
                },
                // 改变全选状态的方法
                changeChecked() {
                    this.carts.forEach((v) => {
                        v.checked = this.allChecked;
                    })
                },
                del(v, k) {
                    axios.get('/deleteCart/' + v.id).then((res) => {
                        if (res.data.valid) {
                            hdjs.swal({
                                text: res.data.message,
                                button: false,
                                icon: 'success'
                            });
                            // 异步删除成功时，页面的数据也对应的要删除才合理
                            this.carts.splice(k, 1);
                        }
                    });
                },
                // 加法运算
                plus(v) {
                    axios.get('/plus/' + v.id).then((res) => {
                        if (res.data.valid) {
                            v.num++;
                            v.xiaoji = v.xiaoji + v.goods.price + v.product.add_price;
                        }
                    });
                },
                // 减法运算
                minus(v) {
                    if (v.num > 1) {
                        axios.get('/minus/' + v.id).then((res) => {
                            if (res.data.valid) {
                                v.num--;
                                v.xiaoji = v.xiaoji - v.goods.price - v.product.add_price;
                            }
                        });
                    }
                }
            },
        });

    });
</script>
{{--为您推荐--}}
<div class="beij_center">
    <div class="investment_f">
        <div class="investment_title">
            <div class="ds_dg on_d">为您推荐</div>
            <div class="ds_dg">最近预览</div>
        </div>
        <div class="investment_con">
            <!---->
            <div class="picScroll_left_s" style="display: block;">
                <div class="hd">
                    <a class="next next_you"></a>
                    <ul></ul>
                    <a class="prev prev_zuo"></a>
                </div>
                <div class="bd">
                    <ul class="picList">
                        <li>
                            <div class="pic"><a href="#" target="_blank"><img
                                        src="{{asset('org/home/images')}}/lieb_tupi3.jpg"/></a></div>
                            <div class="title">
                                <a href="#" target="_blank">喜芬妮春秋桑蚕丝长袖性感蕾丝花边女式睡衣家居服二</a>
                                <div class="jiage_gouw"><span>¥2499.00</span></div>
                                <a href="#" class="cart_scroll_btn">加入购物车</a>
                            </div>
                        </li>
                        <li>
                            <div class="pic"><a href="#" target="_blank"><img
                                        src="{{asset('org/home/images')}}/shangq_3.jpg"/></a></div>
                            <div class="title">
                                <a href="#" target="_blank">喜芬妮春秋桑蚕丝长袖性感蕾丝花边女式睡衣家居服二</a>
                                <div class="jiage_gouw"><span>¥2499.00</span></div>
                                <a href="#" class="cart_scroll_btn">加入购物车</a>
                            </div>
                        </li>
                        <li>
                            <div class="pic"><a href="#" target="_blank"><img
                                        src="{{asset('org/home/images')}}/big_3.jpg"/></a></div>
                            <div class="title">
                                <a href="#" target="_blank">喜芬妮春秋桑蚕丝长袖性感蕾丝花边女式睡衣家居服二</a>
                                <div class="jiage_gouw"><span>¥2499.00</span></div>
                                <a href="#" class="cart_scroll_btn">加入购物车</a>
                            </div>
                        </li>
                        <li>
                            <div class="pic"><a href="#" target="_blank"><img
                                        src="{{asset('org/home/images')}}/xiangqtu_1.jpg"/></a></div>
                            <div class="title">
                                <a href="#" target="_blank">喜芬妮春秋桑蚕丝长袖性感蕾丝花边女式睡衣家居服二</a>
                                <div class="jiage_gouw"><span>¥2499.00</span></div>
                                <a href="#" class="cart_scroll_btn">加入购物车</a>
                            </div>
                        </li>
                        <li>
                            <div class="pic"><a href="#" target="_blank"><img
                                        src="{{asset('org/home/images')}}/lieb_tupi3.jpg"/></a></div>
                            <div class="title">
                                <a href="#" target="_blank">喜芬妮春秋桑蚕丝长袖性感蕾丝花边女式睡衣家居服二</a>
                                <div class="jiage_gouw"><span>¥2499.00</span></div>
                                <a href="#" class="cart_scroll_btn">加入购物车</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!---->
            <div class="picScroll_left_s" style="display: none;">
                <div class="picScroll_left_s_dsl">
                    <div class="dfgc">
                        <ul class="picList">
                            <li>
                                <div class="pic"><a href="#" target="_blank"><img
                                            src="{{asset('org/home/images')}}/lieb_tupi3.jpg"/></a></div>
                                <div class="title">
                                    <a href="#" target="_blank">喜芬妮春秋桑蚕丝长袖性感蕾丝花边女式睡衣家居服二</a>
                                    <div class="jiage_gouw"><span>¥2499.00</span></div>
                                    <a href="#" class="cart_scroll_btn">加入购物车</a>
                                </div>
                            </li>
                            <li>
                                <div class="pic"><a href="#" target="_blank"><img
                                            src="{{asset('org/home/images')}}/big_3.jpg"/></a></div>
                                <div class="title">
                                    <a href="#" target="_blank">喜芬妮春秋桑蚕丝长袖性感蕾丝花边女式睡衣家居服二</a>
                                    <div class="jiage_gouw"><span>¥2499.00</span></div>
                                    <a href="#" class="cart_scroll_btn">加入购物车</a>
                                </div>
                            </li>
                            <li>
                                <div class="pic"><a href="#" target="_blank"><img
                                            src="{{asset('org/home/images')}}/lieb_tupi1.jpg"/></a></div>
                                <div class="title">
                                    <a href="#" target="_blank">喜芬妮春秋桑蚕丝长袖性感蕾丝花边女式睡衣家居服二</a>
                                    <div class="jiage_gouw"><span>¥2499.00</span></div>
                                    <a href="#" class="cart_scroll_btn">加入购物车</a>
                                </div>
                            </li>
                            <li>
                                <div class="pic"><a href="#" target="_blank"><img
                                            src="{{asset('org/home/images')}}/big_3.jpg"/></a></div>
                                <div class="title">
                                    <a href="#" target="_blank">喜芬妮春秋桑蚕丝长袖性感蕾丝花边女式睡衣家居服二</a>
                                    <div class="jiage_gouw"><span>¥2499.00</span></div>
                                    <a href="#" class="cart_scroll_btn">加入购物车</a>
                                </div>
                            </li>
                            <li>
                                <div class="pic"><a href="#" target="_blank"><img
                                            src="{{asset('org/home/images')}}/shangq_3.jpg"></a></div>
                                <div class="title">
                                    <a href="#" target="_blank">喜芬妮春秋桑蚕丝长袖性感蕾丝花边女式睡衣家居服二</a>
                                    <div class="jiage_gouw"><span>¥2499.00</span></div>
                                    <a href="#" class="cart_scroll_btn">加入购物车</a>
                                </div>
                            </li>
                            <li>
                                <div class="pic"><a href="#" target="_blank"><img
                                            src="{{asset('org/home/images')}}/shangq_3.jpg"></a></div>
                                <div class="title">
                                    <a href="#" target="_blank">喜芬妮春秋桑蚕丝长袖性感蕾丝花边女式睡衣家居服二</a>
                                    <div class="jiage_gouw"><span>¥2499.00</span></div>
                                    <a href="#" class="cart_scroll_btn">加入购物车</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {

        /*tab标签切换*/
        function tabs(tabTit, on, tabCon) {
            $(tabCon).each(function () {
                $(this).children().eq(0).show();

            });
            $(tabTit).each(function () {
                $(this).children().eq(0).addClass(on);
            });
            $(tabTit).children().click(function () {
                $(this).addClass(on).siblings().removeClass(on);
                var index = $(tabTit).children().index(this);
                $(tabCon).children().eq(index).show().siblings().hide();
            });
        }

        tabs(".investment_title", "on_d", ".investment_con");

    })
</script>
<script type="text/javascript">
    jQuery(".picScroll_left_s").slide({
        titCell: ".hd ul",
        mainCell: ".bd ul",
        autoPage: true,
        effect: "left",
        autoPlay: true,
        vis: 5,
        trigger: "click"
    });
</script>

<!--底部-->
<div class="dib_beij dib_beij_ger_zhongx">
    <div class="dib_jvz_beij">
        <div class="shangp_baoz">
            <p>本地购物&nbsp;&nbsp;看得见的商品</p>
            <p class="beng1">正品行货&nbsp;&nbsp;购物无忧</p>
            <p class="beng2">低价实惠&nbsp;&nbsp;帮你省钱</p>
            <p class="beng3">本地直发&nbsp;&nbsp;专业配送</p>
        </div>
        <div class="zhongj_youx">
            <div class="lieb_daoh">
                <h4>物流配送</h4>
                <ul>
                    <li><a href="#">配送查询</a></li>
                    <li><a href="#">配送服务</a></li>
                    <li><a href="#">配送费用</a></li>
                    <li><a href="#">配送时效</a></li>
                    <li><a href="#">签收与验货</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>支付与账户</h4>
                <ul>
                    <li><a href="#">货到付款</a></li>
                    <li><a href="#">在线支付</a></li>
                    <li><a href="#">门店支付</a></li>
                    <li><a href="zhangh_anq.html">账户安全</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>购物帮助</h4>
                <ul>
                    <li><a href="#">购物保障</a></li>
                    <li><a href="#">购物流程</a></li>
                    <li><a href="#">焦点问题</a></li>
                    <li><a href="#">联系我们</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>售后服务</h4>
                <ul>
                    <li><a href="#">退换货服务</a></li>
                    <li><a href="#">退款说明</a></li>
                    <li><a href="#">专业维修</a></li>
                    <li><a href="#">延保服务</a></li>
                    <li><a href="#">家电回收</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <div class="kef_dianh">
                    <p>客服电话</p><span>400-6677-937</span>
                </div>
                <div class="kef_dianh kef_dianh_youx">
                    <p>意见收集邮箱</p>
                    <p>Ask@wangid.com</p>
                </div>
            </div>
            <div class="lieb_daoh lieb_daoh_you">
                <div class="erw_ma_beij">
                    <div class="erw_m">
                        <h1><img src="{{asset('org/home/images')}}/mb_wangid.png"></h1>
                        <span>扫码下载通城客户端</span>
                    </div>
                    <div class="erw_m">
                        <h1><img src="{{asset('org/home/images')}}/mb_wangid.png"></h1>
                        <span>扫码下载通城客户端</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="beia_hao">
            <p>京ICP备：14012449号 黔ICP证：B2-20140009号 </p>
            <p class="gonga_bei">京公网安备：11010602030054号</p>
            <div class="renz_">
                <span></span>
                <span class="span1"></span>
                <span class="span2"></span>
                <span class="span3"></span>
            </div>
        </div>
    </div>
</div>
</body>
</html>
