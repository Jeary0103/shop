<!DOCTYPE html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <meta charset="utf-8">
    <title>提交订单——结算页</title>
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/index.css">
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/ziy.css">
    <script src="{{asset('org/home/js')}}/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery.SuperSlide.2.1.1.source.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js/sweetalert.min.js')}}"></script>
</head>
<body>
@include('layouts._home_message')
<!--头部-->
<div id="header">
    <div class="header-box">
        <h3 class="huany"> 欢迎您的到来！</h3>

        <ul class="header-right">
            <li class="denglu dengl_hou">
                <div>
                    <a class="red" href="dengl.html">Hi~{{auth()->user()->name??''}}</a>
                    <i class="icon_plus_nickname"></i>
                </div>
                <div class="dengl_hou_xial_k">
                    <div class="zuid_xiao_toux">
                        <a href="#"><img src="{{auth()->user()->icon??''}}"></a>
                    </div>
                    <div class="huiy_dengj">
                        <a class="tuic_" href="{{route('home.logout')}}">退出</a>
                    </div>
                    <div class="toub_zil_daoh">
                        <a href="#">待处理订单</a>
                        <a href="#">我的收藏</a>
                        <a href="#">个人资料</a>
                    </div>
                </div>
            </li>
            <li class="shu"></li>
            <li class="denglu"><a class="ing_ps" href="#">我的收藏</a></li>
            <li class="shu"></li>
        </ul>
    </div>
</div>
<!--提交订单——结算页-->
<div class="beij_center">
    <div class="dengl_logo">
        <div>
            <img src="{{asset('org/home/images')}}/logo_1.png">
            <h1>结算页</h1>
        </div>
        <div class="stepflex stepflex_2">
            <dl class="normal done ">
                <dt class="s-num">1</dt>
                <dd class="s-text">1.我的购物车<s></s><b></b></dd>
            </dl>
            <dl class="normal doing">
                <dt class="s-num">2</dt>
                <dd class="s-text">2.填写核对订单信息<s></s><b></b></dd>
            </dl>
            <dl class="normal ">
                <dt class="s-num">3</dt>
                <dd class="s-text">3.成功提交订单<s></s><b></b></dd>
            </dl>
        </div>
    </div>
</div>
<div class="beij_center">
    <div class="checkout-tit">
        <span class="tit-txt">填写并核对订单信息</span>
    </div>
    <div class="checkout_steps">
        <div class="step-tit">
            <h3>收货人信息</h3>
            <div class="extra_r">
                <a href="{{route('home.center','address')}}" class="ftx-05 J_consignee_global">新增/编辑收货地址</a>
            </div>
        </div>
        {{--收货人地址--}}
        <div class="jies_y_shouh_diz shiq_1">
            <ul>
                @foreach($addresses as $address)
                    <li address_id="{{$address['id']}}" class="{{$address['is_default']?'cur':''}}"
                        style="cursor: pointer;">
                        <div class="dangq_xuanz_diz">当前地址</div>
                        <span class="linkman">{{$address['name']}}</span>
                        <span class="area">{{$address['area']}} {{$address['address']}}</span>
                        <span class="phone">{{$address['phone']}}</span>
                        <div class="bianji_yv_shanc">
                            <a href="{{route('home.setDefault',$address)}}">设为默认</a>
                            <a href="javascript:;">编辑</a>
                        </div>
                    </li>
                @endforeach
            </ul>
            <div class="addr-switch cur_e">
                <p><span>更多地址</span><b></b></p>
                <p class="jiant_xiangs"><span>收起更多</span><b></b></p>
            </div>
        </div>
        <div class="jies_y_shouh_diz shiq_2">
            <ul>
                <li class="zhif_fangs cur">
                    <div class="dangq_xuanz_diz">微信支付</div>
                </li>
            </ul>
        </div>
        <div class="jies_y_shouh_diz shiq_3">
            <div class="dangq_xuanz_diz"><strong>确认订单信息</strong></div>
        </div>
        <div class="shopping_list">
            <div class="goods_list" style="width:100%;">
                @foreach($carts as $cart)
                    <div class="goods_list_neik">
                        <div class="goods_item">
                            <div class="p_img" style="width:60px;height:60px;"><a href="#"><img
                                        src="{{$cart->goods->pics[0]}}"></a></div>
                            <div class="goods_msg" style="width: 1020px;">
                                <div class="p_name">
                                    <a href="#">{{$cart->goods->title}} {{$cart->product->attrs}}</a>
                                </div>
                                <div class="p_price">
                                    <span class="jq_price">￥{{$cart->goods->price}}</span>
                                    <span>x{{$cart['num']}}</span>
                                    <span>有货</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
    <!--收费明细-->
    <div class="trade_foot_detail_com" style="width:99%" ;>
        <div class="dsgs">
            <div class="qianq_mx">
                <div class="jiaq_meih">
                    <span class="xiangq_leib"> 共<em class="goumai_ges">{{count($ids)}}</em> 件商品,应付总额：</span>
                    <em class="goum_zongj zhongf_jine">￥{{$totalPrice}}</em>
                </div>
            </div>
        </div>

        <div class="zuiz_diz">
            <span class="jisongzhi">寄送至：{{$defaultAddress['area']}}{{$defaultAddress['address']}}</span>
            <span class="shouhuoren"> 收货人：{{$defaultAddress['name']}} {{$defaultAddress['phone']}}</span>
        </div>
    </div>
    <div class="tij_dingd_ann">
        <a href="javascript:;" onclick="storeOrder()">提交订单</a>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $(".shiq_1 ul li").hide().first().show()
        $(".cur_e p").hide().first().show()
        $(".cur_e p:first").click(
            function () {
                $(".shiq_1 ul li").show()
                $(".cur_e p").hide().last().show()
            }
        )
        $(".cur_e p:last").click(
            function () {
                $(".shiq_1 ul li").hide().first().show()
                $(".cur_e p").hide().first().show()
            }
        )
    })
    $(function () {
        $(".shiq_2 ul li").hide().first().show()
        $(".cur_e_1 p").hide().first().show()
        $(".cur_e_1 p:first").click(
            function () {
                $(".shiq_2 ul li").show()
                $(".cur_e_1 p").hide().last().show()
            }
        )
        $(".cur_e_1 p:last").click(
            function () {
                $(".shiq_2 ul li").hide().first().show()
                $(".cur_e_1 p").hide().first().show()
            }
        )
    })
    //cur
</script>

<!--底部-->
<div class="dib_beij">
    <div class="dib_jvz_beij">
        <div class="shangp_baoz">
            <p>本地购物&nbsp;&nbsp;看得见的商品</p>
            <p class="beng1">正品行货&nbsp;&nbsp;购物无忧</p>
            <p class="beng2">低价实惠&nbsp;&nbsp;帮你省钱</p>
            <p class="beng3">本地直发&nbsp;&nbsp;专业配送</p>
        </div>
        <div class="zhongj_youx">
            <div class="lieb_daoh">
                <h4>物流配送</h4>
                <ul>
                    <li><a href="javascript:;">配送查询</a></li>
                    <li><a href="javascript:;">配送服务</a></li>
                    <li><a href="javascript:;">配送费用</a></li>
                    <li><a href="javascript:;">配送时效</a></li>
                    <li><a href="javascript:;">签收与验货</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>支付与账户</h4>
                <ul>
                    <li><a href="javascript:;">货到付款</a></li>
                    <li><a href="javascript:;">在线支付</a></li>
                    <li><a href="javascript:;">门店支付</a></li>
                    <li><a href="javascript:;">账户安全</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>购物帮助</h4>
                <ul>
                    <li><a href="javascript:;">购物保障</a></li>
                    <li><a href="javascript:;">购物流程</a></li>
                    <li><a href="javascript:;">焦点问题</a></li>
                    <li><a href="javascript:;">联系我们</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>售后服务</h4>
                <ul>
                    <li><a href="javascript:;">退换货服务</a></li>
                    <li><a href="javascript:;">退款说明</a></li>
                    <li><a href="javascript:;">专业维修</a></li>
                    <li><a href="javascript:;">延保服务</a></li>
                    <li><a href="javascript:;">家电回收</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <div class="kef_dianh">
                    <p>客服电话</p><span>15110853679</span>
                </div>
                <div class="kef_dianh kef_dianh_youx">
                    <p>意见收集邮箱</p>
                    <p>Ask@wangid.com</p>
                </div>
            </div>
            <div class="lieb_daoh lieb_daoh_you">
                <div class="erw_ma_beij">
                    <div class="erw_m">
                        <h1><img src="{{asset('org/home/images')}}/mb_wangid.png"></h1>
                        <span>扫码下载通城客户端</span>
                    </div>
                    <div class="erw_m">
                        <h1><img src="{{asset('org/home/images')}}/mb_wangid.png"></h1>
                        <span>扫码下载通城客户端</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="beia_hao">
            <p>京ICP备：14012449号 黔ICP证：B2-20140009号 </p>
            <p class="gonga_bei">京公网安备：11010602030054号</p>
            <div class="renz_">
                <span></span>
                <span class="span1"></span>
                <span class="span2"></span>
                <span class="span3"></span>
            </div>
        </div>
    </div>
</div>
@include('layouts._hdjs')
<script>
    $(function () {
        $('.jies_y_shouh_diz').find('ul li').click(function () {
            $(this).addClass('cur').siblings().removeClass('cur');
            $('.jisongzhi').text('寄送至: ' + $(this).find('.area').text());
            $('.shouhuoren').text('收货人: ' + $(this).find('.linkman').text() + ' ' + $(this).find('.phone').text());
        });
    });

    // 保存订单的方法，我们这里因为没有固定的表单数据，所以这里采用了异步保存数据的方法
    function storeOrder() {
        require(['hdjs', 'axios'], function (hdjs, axios) {
            var ids = "{{$str_ids}}";
            axios.post('{{route('home.order.store')}}', {
                // 还是购物车提交过来的那几条购物车编号，目的是为了求总价
                ids: ids,
                // 把被选中的地址的id带到服务器端，往表里面进行填充
                address_id: $('.jies_y_shouh_diz').find('ul li.cur').attr('address_id')
            }).then((res) => {
                // console.log(res);
                if (res.data.code) {
                    swal({
                        title: '提交成功！',
                        icon: 'success',
                    });
                    setTimeout(function () {
                        location.href = '/pay/' + res.data.data.order_bianhao;
                    }, 2000);
                }
            });
        });

    }
</script>
</body>
</html>
