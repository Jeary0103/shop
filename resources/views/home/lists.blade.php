@extends('home.layouts.master')
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/index.css">
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/ziy.css">
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery-1.11.1.min.js"></script>
@endpush
@section('content')
    <!--热卖推荐-->
    <div class="nSearchWarp">
        <div class="hot-tj">
            <span class="icon_tj">热卖<br>推荐</span>
            <ul class="hot-list clearfix">
                <li class="item asynPriceBox">
                    <p class="pic">
                        <a target="_blank" href="#"><img src="{{asset('org/home/images')}}/lieb_tupi1.jpg" alt=""></a>
                    </p>
                    <p class="name"><a href="#" title=" 联想(lenovo) 小新潮5000 15.6英寸轻薄笔记本电脑"> 联想(lenovo) 小新潮5000
                            15.6英寸轻薄笔记本电脑 </a></p>
                    <p class="price asynPrice">¥3999.00</p>
                    <p class="btn"><a class="buy" href="#">立即抢购</a></p>
                </li>
                <li class="item asynPriceBox">
                    <p class="pic">
                        <a target="_blank" href="#"><img src="{{asset('org/home/images')}}/lieb_tupi2.jpg" alt=""></a>
                    </p>
                    <p class="name"><a href="#" title=" 联想(lenovo) 小新潮5000 15.6英寸轻薄笔记本电脑"> 联想(lenovo) 小新潮5000
                            15.6英寸轻薄笔记本电脑 </a></p>
                    <p class="price asynPrice">¥3999.00</p>
                    <p class="btn"><a class="buy" href="#">立即抢购</a></p>
                </li>
                <li class="item asynPriceBox">
                    <p class="pic">
                        <a target="_blank" href="#"><img src="{{asset('org/home/images')}}/lieb_tupi3.jpg" alt=""></a>
                    </p>
                    <p class="name"><a href="#" title=" 联想(lenovo) 小新潮5000 15.6英寸轻薄笔记本电脑"> 联想(lenovo) 小新潮5000
                            15.6英寸轻薄笔记本电脑 </a></p>
                    <p class="price asynPrice">¥3999.00</p>
                    <p class="btn"><a class="buy" href="#">立即抢购</a></p>
                </li>
                <li class="item asynPriceBox">
                    <p class="pic">
                        <a target="_blank" href="#"><img src="{{asset('org/home/images')}}/lieb_tupi1.jpg" alt=""></a>
                    </p>
                    <p class="name"><a href="#" title=" 联想(lenovo) 小新潮5000 15.6英寸轻薄笔记本电脑"> 联想(lenovo) 小新潮5000
                            15.6英寸轻薄笔记本电脑 </a></p>
                    <p class="price asynPrice">¥3999.00</p>
                    <p class="btn"><a class="buy" href="#">立即抢购</a></p>
                </li>
            </ul>
        </div>
    </div>


    <div class="shangp_lieb_jvz">
        <script type="text/javascript">
            function xians() {
                var ddd = document.getElementById('yingc');
                if (ddd.style.display == 'block') {
                    ddd.style.display = 'none';
                }
                else {
                    ddd.style.display = 'block';
                }
            }
        </script>
        <div class="shangp_lieb_yi">
            <div class="filter_yi">
                <div class="f_line">
                    <div class="f_sort">
                        <a href="{{route('home.index')}}" class="curr_1"><label>所在分类</label> <i></i></a>
                        @foreach($fatherCategory as $category)
                            <a href="{{route('home.lists',$category)}}" class="curr_2">{{$category['title']}}<i></i></a>
                        @endforeach
                    </div>
                    <div class="f_pager" id="J_topPage">
           			<span class="fp_text">
               			<b>1</b><em>/</em><i>166</i>
          			</span>
                        <a class="fp_prev disabled" href="#"> &lt; </a>
                        <a class="fp_next" href="#"> &gt; </a>
                    </div>

                </div>
            </div>
            <div class="shnagp_list_v1">
                <ul>
                    {{--列表页循环开始--}}
                    @foreach($goods as $good)
                        <li>
                            <div class="lieb_neir_kuang">
                                <div class="lieb_img">
                                    <a href="{{route('home.content',$good)}}"><img src="{{$good['pics'][0]}}"
                                                                                   style="width: 276px;height: 209px"></a>
                                    <div class="p_focus"><a class="J_focus" href="#"><i></i>关注</a></div>
                                </div>
                                <div class="lieb_text">
                                    <div class="p_price">
                                        <strong class="J_price"><em>¥</em><i>{{$good['price']}}</i><em
                                                class="shangp_yuanj zuo_ji">¥</em><i
                                                class="shangp_yuanj">{{$good['price']}}</i></strong>
                                    </div>
                                </div>
                                <div class="shangp_biaot_"><a
                                        href="{{route('home.content',$good)}}">{{$good['title']}}</a></div>
                                <div class="lieb_dianp_mingc">
                                    <div class="zuo_mingc">
                                        <div class="p_icons">
                                            <i class="icon_grou_1" data-tips="本地商品"><img
                                                    src="{{asset('org/home/images')}}/bend.png"></i>
                                            <i class="icon_grou_2" data-tips="商品特价出售">特价</i>
                                        </div>
                                    </div>
                                    <div class="you_pingj">
                                        <p>已有评价</p>
                                        <span><a href="#"><em>99+</em></a> 人</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    {{--列表页循环结束--}}
                </ul>
                <div class="page clearfix">
                    <div class="p-wrap" id="J_bottomPage">
		            <span class="p-num">
						<a class="pn-prev disabled" href="undefined&amp;ms=5"><i>&lt;</i><em>上一页</em></a>
						<a href="#" class="curr_3">1</a>
		                <b class="pn-break hide"> …</b>
		                <a href="/#" class="hide ">-2</a>
		                <a href="/#" class="hide ">-1</a>
		                <a href="#" class="hide ">0</a>
		                <a href="#" class="hide curr_3">1</a>
		                <a href="#" class=" ">2</a>
		                <a href="#" class=" ">3</a>
		                <b class="pn-break "> …</b>
		                <a href="#" class="">166</a>
						<a class="pn-next" href="#">下一页<i>&gt;</i></a>
					</span>
                        <span class="p-skip">
		                <em>共<b>166</b>页&nbsp;&nbsp;到第</em>
		                <input class="input-txt" value="1">
		                <em>页</em>
		                <a class="btn btn-default" href="javascript:page_jump();">确定</a>
		            </span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--列表一结束-->
    <!--猜你喜欢-->
    <div class="cain_xih">
        <div class="mt">
            <h2 class="title">猜你喜欢</h2>
            <div class="extra">
                <a href="#" class="change"><i class="ico"></i><span class="txt">换一批</span></a>
            </div>
        </div>
        <ul class="cain_xihuan_neir">
            <li>
                <div class="item_pic"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi1.jpg"></a></div>
                <div class="cain_xih_biaot"><a href="#">伊秋梦紫 2017夏季新款韩版小清新中长款碎花雪纺连衣裙8819(米白色 XXL棉麻连衣裙，舒适透气，MM</a></div>
                <div class="cain_xih_jiaq"><p>￥560.00</p></div>
            </li>
            <li>
                <div class="item_pic"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi2.jpg"></a></div>
                <div class="cain_xih_biaot"><a href="#">伊秋梦紫 2017夏季新款韩版小清新中长款碎花雪纺连衣裙8819(米白色 XXL棉麻连衣裙，舒适透气，MM</a></div>
                <div class="cain_xih_jiaq"><p>￥560.00</p></div>
            </li>
            <li>
                <div class="item_pic"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi3.jpg"></a></div>
                <div class="cain_xih_biaot"><a href="#">伊秋梦紫 2017夏季新款韩版小清新中长款碎花雪纺连衣裙8819(米白色 XXL棉麻连衣裙，舒适透气，MM</a></div>
                <div class="cain_xih_jiaq"><p>￥560.00</p></div>
            </li>
            <li>
                <div class="item_pic"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi1.jpg"></a></div>
                <div class="cain_xih_biaot"><a href="#">伊秋梦紫 2017夏季新款韩版小清新中长款碎花雪纺连衣裙8819(米白色 XXL棉麻连衣裙，舒适透气，MM</a></div>
                <div class="cain_xih_jiaq"><p>￥560.00</p></div>
            </li>
            <li>
                <div class="item_pic"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi3.jpg"></a></div>
                <div class="cain_xih_biaot"><a href="#">伊秋梦紫 2017夏季新款韩版小清新中长款碎花雪纺连衣裙8819(米白色 XXL棉麻连衣裙，舒适透气，MM</a></div>
                <div class="cain_xih_jiaq"><p>￥560.00</p></div>
            </li>
            <li>
                <div class="item_pic"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi2.jpg"></a></div>
                <div class="cain_xih_biaot"><a href="#">伊秋梦紫 2017夏季新款韩版小清新中长款碎花雪纺连衣裙8819(米白色 XXL棉麻连衣裙，舒适透气，MM</a></div>
                <div class="cain_xih_jiaq"><p>￥560.00</p></div>
            </li>
        </ul>
    </div>
@endsection
