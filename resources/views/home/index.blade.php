@extends('home.layouts.master')
@section('content')
    <!--轮播图上方导航栏   左侧导航栏-->
    <div id="navv">
        <div class="focus">
            <div class="focus-a">
                <div class="fouc-font"><a href="">全部商品分类</a></div>
            </div>
            <!--左边导航-->
            <div class="dd-inner">
                {{--这里循环的是所有的一级分类 得到的是每一个一级分类--}}
                @foreach($categories as $category)
                    <div class="font-item">
                        {{--一二级分类--}}
                        <div class="item fore1">
                            <h3>
                                <a class="da_zhu" href="{{route('home.lists',$category)}}"
                                   target="_blank">{{$category->title}}</a>
                                <p>
                                    @foreach($category->category as $two)
                                        <a href="{{route('home.lists',$two)}}" target="_blank">{{$two['title']}}</a>
                                    @endforeach
                                </p>
                            </h3>
                            <i>></i>
                        </div>
                        {{-- 二三级分类--}}
                        <div class="font-item1">
                            <div class="font-lefty">
                                @foreach($category->category as $two)
                                    <dl class="fore1">
                                        <dt><a href="{{route('home.lists',$two)}}" target="_blank">{{$two['title']}}
                                                <i>></i></a></dt>
                                        <dd>
                                            @foreach($two->category as $three)
                                                <a href="{{route('home.lists',$three)}}"
                                                   target="_blank">{{$three['title']}}</a>
                                            @endforeach
                                        </dd>
                                    </dl>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>


    <!--轮播图-->
    <div id="lunbo">
        <ul id="one">
            @foreach($slides as $slide)
                <li><a href=""><img src="{{$slide['image']}}"></a></li>
            @endforeach
        </ul>
        <ul id="two">
            <li class="on">1</li>
            <li>2</li>
            <li>3</li>
        </ul>
        <!--轮播图左右箭头-->
        <div class="slider-page">
            <a href="javascript:void(0)" id="left"><</a>
            <a href="javascript:void(0)" id="right">></a>
        </div>
    </div>

    <!--快速通道栏存放着最新的商品表里面的4条数据-->
    <div class="kuanjlan">
        <ul class="clearfix">
            @foreach($share_goods as $share_good)
                <li>
                    <div class="list-li-box" style="text-align: center;">
                        <a class="list-img" href="#" data-code="index01004-1" target="_blank">
                            <img src="{{$share_good['pics'][0]}}" style="width:294px;height:152px;">
                            <span class="list-bg"></span>
                            <div class="list-cont">
                                <p class="cont-tile">{{$share_good['title']}}</p>
                            </div>
                        </a>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>

    <!--层次是按照一级分类区分的，所以我们要显示层次，就应该循环一级分类的数据-->
    @foreach($categories as $k=>$category)
        @if($k<2)
            <div class="chengc_jvz" style="margin:0px auto;">
                <div class="slideTxtBox">
                    {{--每一个层次的tab导航部分，包含一级和二级分类--}}
                    <div class="hd">
                        <h1>
                            {{--左侧：一级分类名称--}}
                            <p>{{$category['title']}}</p>
                            <p class="yingw_"></p>
                        </h1>
                        <ul>
                            {{-- 右侧：二级分类名称 --}}
                            @foreach($category->category as $two)
                                <li>{{$two['title']}}</li>
                            @endforeach
                        </ul>
                    </div>
                    {{--二级分类的下的商品列表--}}
                    <div class="bd">
                        {{--这里这个大块跟tab的二级菜单数量一样，所以这里需要循环二级分类--}}
                        {{--但是展示数据时，某一个二级分类下的三级分类下的数据，所以在模型里面要用二级分类找它的子类--}}
                        {{--然后求出3级分类下的商品信息--}}
                        @foreach($category->category as $two)
                            {{--ul是每一个tab的内容--}}
                            <ul>
                                {{--主体左侧--}}
                                <div class="pangb_daoh">
                                    <h1><a href="#"><img src="{{asset('org/home')}}/images/de111.jpg"></a></h1>
                                    <div class="kuas_daoh_houm">
                                        @foreach($two->category as $three)
                                            <a href="{{route('home.lists',$three)}}" target="_blank">{{$three['title']}}</a>
                                        @endforeach
                                    </div>
                                </div>
                                {{--主体右侧--}}
                                <div class="you_lirb you_lirb_hou">
                                    <div class="shang_buf">
                                        {{--获取所有二级分类下的所有数据，而且是每一个三级分类下找出一个商品数据--}}
                                        @foreach($two->getGoods() as $good)
                                            <div class="you_shangp_lieb ">
                                                <a href="{{route('home.content',$good)}}" target="_blank">
                                                    <img class="you_tup_k" src="{{$good['pics'][0]}}">
                                                </a>
                                                <a href="{{route('home.content',$good)}}" target="_blank"
                                                   class="_you_neir_biaot">{{$good['title']}}</a>
                                                <span>¥{{$good['price']}}</span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </ul>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    @endforeach

    <script type="text/javascript">jQuery(".slideTxtBox").slide();</script>
    <script type="text/javascript">jQuery(".slideTxtBox2").slide();</script>
    <script type="text/javascript">jQuery(".slideTxtBox3").slide();</script>
    <script type="text/javascript">jQuery(".slideTxtBox4").slide();</script>
    <!--广告图-->
    <div class="guangg_tu">
        <a href="#"><img src="{{asset('org/home')}}/images/guang_tu.jpg"></a>
    </div>
    <!--特色商品/ 热门商品-->
    <div class="tes_shnagp_beij">
        {{--特色商品--}}
        <div class="tes_shangp">
            <div class="neir_biaot">
                <p>特色商品</p>
                <a href="#">MORE+</a>
            </div>
            <div class="tes_shangp_neir_k">
                {{--左侧--}}
                <div class="tes_dat">
                    <a href="{{route('home.content',$share_goods[0]['id'])}}" target="_blank">
                        <h1><img class="tes_dat_dongh" src="{{$share_goods[0]['pics'][0]}}"/></h1>
                        <h2>{{$share_goods[0]['title']}}</h2>
                        <span>¥{{$share_goods[0]['price']}}</span>
                    </a>
                </div>
                {{--右侧--}}
                <div class="tes_xiaot_beij">
                    <div class="tes_xiaot_shang">
                        <div class="tes_xiaot_neir">
                            <a href="{{route('home.content',$goods[0]['id'])}}" target="_blank">
                                <h1><img class="tes_xiaot_dongh" src="{{$goods[0]['pics'][0]}}">
                                </h1>
                                <h2>{{$goods[0]['title']}}</h2>
                                <span>¥{{$goods[0]['price']}}</span>
                            </a>
                        </div>
                        <div class="tes_xiaot_neir tes_xiaot_wubian_kuang">
                            <a href="{{route('home.content',$goods[1]['id'])}}" target="_blank">
                                <h1><img class="tes_xiaot_dongh"
                                         src="{{$goods[1]['pics'][0]}}">
                                </h1>
                                <h2>{{$goods[1]['title']}}</h2>
                                <span>¥{{$goods[1]['price']}}</span>
                            </a>
                        </div>
                    </div>
                    <div class="tes_xiaot_shang tes_xiaot_xia">
                        <div class="tes_xiaot_neir">
                            <a href="{{route('home.content',$goods[2]['id'])}}" target="_blank">
                                <h1><img class="tes_xiaot_dongh"
                                         src="{{$goods[2]['pics'][0]}}">
                                </h1>
                                <h2>{{$goods[2]['title']}}</h2>
                                <span>¥{{$goods[2]['price']}}</span>
                            </a>
                        </div>
                        <div class="tes_xiaot_neir tes_xiaot_wubian_kuang">
                            <a href="{{route('home.content',$goods[3]['id'])}}" target="_blank">
                                <h1><img class="tes_xiaot_dongh"
                                         src="{{$goods[3]['pics'][0]}}">
                                </h1>
                                <h2>{{$goods[3]['title']}}</h2>
                                <span>¥{{$goods[3]['price']}}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--热门商品--}}
        <div class="rem_shangp">
            <div class="neir_biaot">
                <p>热门商品</p>
                <a href="#">MORE+</a>
            </div>
            <div class="rem_shangp_beij_k">
                <!---->
                <div class="picScroll_left">
                    <div class="hd">
                        <ul></ul>
                    </div>
                    <div class="bd">
                        <ul class="picList">
                            @foreach($commend_goods as $commend_good)
                                <li>
                                    <div class="pic">
                                        <a href="{{route('home.content',$commend_good['id'])}}" target="_blank">
                                            <img src="{{$commend_good['pics'][0]}}"/>
                                        </a>
                                    </div>
                                    <div class="title">
                                        <a href="{{route('home.content',$commend_good['id'])}}" target="_blank">
                                            {{$commend_good['title']}}
                                        </a>
                                        <span>¥ {{$commend_good['price']}}</span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!---->
            </div>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(".picScroll_left").slide({
            titCell: ".hd ul",
            mainCell: ".bd ul",
            autoPage: true,
            effect: "left",
            autoPlay: true,
            vis: 2,
            trigger: "click"
        });
    </script>
@endsection
