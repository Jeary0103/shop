<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>在线商城</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/index.css">
    <style>
        .tes_shangp_neir_k h2 {
            font-size: 12px;
        }
        .rem_shangp_beij_k img{
            border:1px solid #fff0ff;
            width:214px;
            height:204px;
        }
        .rem_shangp_beij_k .title{
            max-width: 214px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
    </style>

    @stack('css')
    <script src="{{asset('org/home/js')}}/jquery-1.11.3.min.js"></script>
    <script src="{{asset('org/home/js')}}/index.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery.SuperSlide.2.1.1.source.js"></script>
    <script src="https://cdn.bootcss.com/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @stack('js')
</head>
<body>
@include('layouts._home_message')
<!--侧边-->
<div class="jdm-toolbar-wrap J-wrap">
    <div class="jdm-toolbar J-toolbar">
        <div class="jdm-toolbar-panels J-panel"></div>
        <div class="jdm-toolbar-tabs J-tab">
            <div data-type="bar" class="J-trigger jdm-toolbar-tab jdm-tbar-tab-ger">
                <i class="tab-ico"></i>
                <a href="{{route('home.center','basic')}}"><em class="tab-text">个人中心</em></a>
            </div>
            <div data-type="bar" class="J-trigger jdm-toolbar-tab jdm-tbar-tab-cart">
                <i class="tab-ico"></i>
                <a href="{{route('home.myCart')}}"><em class="tab-text">购物车</em></a>
            </div>
            <div data-type="bar" clstag="h|keycount|cebianlan_h_follow|btn"
                 class="J-trigger jdm-toolbar-tab jdm-tbar-tab-follow" data-name="follow" data-login="true">
                <i class="tab-ico"></i>
                <em class="tab-text">我的关注</em>
            </div>
        </div>
        <div class="J-trigger jdm-toolbar-tab jdm-tbar-tab-message" data-name="message"><a target="_blank" href="#">
                <i class="tab-ico"></i>
                <em class="tab-text">我的消息
                </em></a>
        </div>
    </div>
    <div class="jdm-toolbar-footer">
        <div data-type="link" id="#top" class="J-trigger jdm-toolbar-tab jdm-tbar-tab-top">
            <a href="#" clstag="h|keycount|cebianlan_h|top">
                <i class="tab-ico"></i>
                <em class="tab-text">顶部</em>
            </a>
        </div>
    </div>
    <div class="jdm-toolbar-mini"></div>
    <div id="J-toolbar-load-hook" clstag="h|keycount|cebianlan_h|load"></div>
</div>
{{--引入头部位置--}}
<div id="header">
    <div class="header-box">
        <h3 class="huany">欢迎您的到来！</h3>
        @auth
            <ul class="header-right">
                <li class="denglu">Hi~<a class="red"
                                         href="{{route('home.center','index')}}">{{auth()->user()->name}}</a> <a
                        href="{{route('home.logout')}}">【退出登录】</a></li>
            </ul>
        @else
            <ul class="header-right">
                <li class="denglu">Hi~<a class="red" href="{{route('home.login')}}">请登录!</a> <a
                        href="{{route('home.regist')}}">[免费注册]</a></li>
            </ul>
        @endauth
    </div>
</div>
<!--搜索栏-->
<div class="toub_beij">
    <div class="logo"><a href="./"><img src="{{asset('org/home')}}/images/logo11.png"></a></div>
    <div class="search">
        <input type="text" value="家电一折抢" class="text" id="textt">
        <button class="button">搜索</button>
    </div>
    <div class="right">
        <i class="gw-left"></i>
        <i class="gw-right"></i>
        <div class="sc">
            @if(isset($carts))
                <i class="gw-count">{{count($carts)}}</i>
            @endif
            <i class="sd"></i>
        </div>
        <a href="{{route('home.myCart')}}">我的购物车</a>
        <div class="dorpdown-layer">
            <ul>
                @if(isset($carts) && count($carts)>0)
                    <li class="meiyou" style="width:90%;">
                        <span style="font-size: 14px;font-weight: bold"> 最近加入的宝贝</span>
                    </li>
                @else
                    <li class="meiyou" style="width:90%;">
                        <span style="font-size: 14px;font-weight: bold"> 您还没有收藏任何商品</span>
                    </li>
                @endif
                @if(isset($carts))
                    @foreach($carts as $k=>$cart)
                            <li class="meiyou"
                                style="width:90%;overflow: hidden;white-space:nowrap;text-overflow:ellipsis;">
                                <img style="width:57px;height:49px;" src="{{$cart->goods->pics[0]}}">
                                <span><a target="_blank" href="{{route('home.myCart')}}"
                                         style="font-size: 12px;">{{$cart->goods->title}}</a></span>
                            </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
    <div class="hotwords">
        <a class="biaoti">热门搜索：</a>
        <a href="#">新款连衣裙</a>
        <a href="#">四件套</a>
        <a href="#">潮流T恤</a>
        <a href="#">时尚女鞋</a>
        <a href="#">乐1</a>
        <a href="#">特步女鞋</a>
        <a href="#">威士忌</a>
    </div>
</div>

{{--主体部分占位--}}
@yield('content')

<!--底部-->
<div class="dib_beij">
    <div class="dib_jvz_beij">
        <div class="shangp_baoz">
            <p>本地购物&nbsp;&nbsp;看得见的商品</p>
            <p class="beng1">正品行货&nbsp;&nbsp;购物无忧</p>
            <p class="beng2">低价实惠&nbsp;&nbsp;帮你省钱</p>
            <p class="beng3">本地直发&nbsp;&nbsp;专业配送</p>
        </div>
        <div class="zhongj_youx">
            <div class="lieb_daoh">
                <h4>物流配送</h4>
                <ul>
                    <li><a href="javascript:;">配送查询</a></li>
                    <li><a href="javascript:;">配送服务</a></li>
                    <li><a href="javascript:;">配送费用</a></li>
                    <li><a href="javascript:;">配送时效</a></li>
                    <li><a href="javascript:;">签收与验货</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>支付与账户</h4>
                <ul>
                    <li><a href="javascript:;">货到付款</a></li>
                    <li><a href="javascript:;">在线支付</a></li>
                    <li><a href="javascript:;">门店支付</a></li>
                    <li><a href="javascript:;">账户安全</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>购物帮助</h4>
                <ul>
                    <li><a href="javascript:;">购物保障</a></li>
                    <li><a href="javascript:;">购物流程</a></li>
                    <li><a href="javascript:;">焦点问题</a></li>
                    <li><a href="javascript:;">联系我们</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <h4>售后服务</h4>
                <ul>
                    <li><a href="javascript:;">退换货服务</a></li>
                    <li><a href="javascript:;">退款说明</a></li>
                    <li><a href="javascript:;">专业维修</a></li>
                    <li><a href="javascript:;">延保服务</a></li>
                    <li><a href="javascript:;">家电回收</a></li>
                </ul>
            </div>
            <div class="lieb_daoh">
                <div class="kef_dianh">
                    <p>客服电话</p><span>15110853679</span>
                </div>
                <div class="kef_dianh kef_dianh_youx">
                    <p>意见收集邮箱</p>
                    <p>237618121@qq.com</p>
                </div>
            </div>
            <div class="lieb_daoh lieb_daoh_you">
                <div class="erw_ma_beij">
                    <div class="erw_m">
                        <h1><img style="width:127px;height:127px;" src="/images/nopic.jpg"></h1>
                        <span>扫码下载通城客户端</span>
                    </div>
                    <div class="erw_m">
                        <h1><img src="/images/nopic.jpg"></h1>
                        <span>扫码下载通城客户端</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
