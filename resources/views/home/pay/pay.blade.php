<!DOCTYPE html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <meta charset="utf-8">
    <title>支付页面</title>
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/index.css">
    <link rel="stylesheet" type="text/css" href="{{asset('org/home/css')}}/ziy.css">
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{asset('org/home/js')}}/jquery.SuperSlide.2.1.1.source.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

</head>
<body>
<!--侧边-->
<div class="jdm-toolbar-wrap J-wrap">
    <div class="jdm-toolbar J-toolbar">
        <div class="jdm-toolbar-panels J-panel"></div>
        <div class="jdm-toolbar-tabs J-tab">
            <div data-type="bar" class="J-trigger jdm-toolbar-tab jdm-tbar-tab-ger">
                <i class="tab-ico"></i>
                <em class="tab-text">购物车</em>
            </div>
            <div data-type="bar" class="J-trigger jdm-toolbar-tab jdm-tbar-tab-cart">
                <i class="tab-ico"></i>
                <em class="tab-text">购物车</em>
            </div>
            <div data-type="bar" clstag="h|keycount|cebianlan_h_follow|btn"
                 class="J-trigger jdm-toolbar-tab jdm-tbar-tab-follow" data-name="follow" data-login="true">
                <i class="tab-ico"></i>
                <em class="tab-text">我的关注</em>
            </div>
        </div>
        <div class="J-trigger jdm-toolbar-tab jdm-tbar-tab-message" data-name="message"><a target="_blank" href="#">
                <i class="tab-ico"></i>
                <em class="tab-text">我的消息
                </em></a>
        </div>
    </div>
    <div class="jdm-toolbar-footer">
        <div data-type="link" id="#top" class="J-trigger jdm-toolbar-tab jdm-tbar-tab-top">
            <a href="#" clstag="h|keycount|cebianlan_h|top">
                <i class="tab-ico"></i>
                <em class="tab-text">顶部</em>
            </a>
        </div>
    </div>
    <div class="jdm-toolbar-mini"></div>
    <div id="J-toolbar-load-hook" clstag="h|keycount|cebianlan_h|load"></div>
</div>
<!--头部-->
<div id="header">
    <div class="header-box">
        <h3 class="huany">欢迎您的到来！</h3>
        <ul class="header-right">
            @auth
                <li class="denglu">
                    Hi~<a class="red" href="{{route('home.center','basic')}}">{{auth()->user()->name}}</a>
                    <a href="{{route('home.logout')}}">【退出登录】</a>
                </li>
            @else
                <li class="denglu">
                    Hi~<a class="red" href="{{route('home.login')}}">请登录!</a>
                    <a href="{{route('home.regist')}}">[免费注册]</a>
                </li>
            @endauth
            <li class="shu"></li>
            <li class="denglu"><a class="ing_ps" href="#">我的收藏</a></li>
        </ul>
    </div>
</div>
<!--搜索栏-->
<div class="toub_beij">
    <div class="logo"><a href="./"><img src="{{asset('org/home/images/logo11.png')}}"></a></div>
    <div class="search">
        <input type="text" value="家电一折抢" class="text" id="textt">
        <button class="button">搜索</button>
    </div>

    <div class="hotwords">
        <a class="biaoti">热门搜索：</a>
        <a href="#">新款连衣裙</a>
        <a href="#">四件套</a>
        <a href="#">潮流T恤</a>
        <a href="#">时尚女鞋</a>
        <a href="#">乐1</a>
        <a href="#">特步女鞋</a>
        <a href="#">威士忌</a>
    </div>
</div>


<hr/>
<script type="text/javascript">
    (function () {
        var $subblock = $(".subpage"), $head = $subblock.find('h2'), $ul = $("#proinfo"), $lis = $ul.find("li"),
            inter = false;
        $head.mouseover(function (e) {
            e.stopPropagation();
            if (!inter) {
                $ul.show();
            } else {
                $ul.hide();
            }
            inter = !inter;
        });

        $ul.mouseover(function (event) {
            event.stopPropagation();
        });

        $(document).mouseover(function () {
            $ul.hide();
            inter = !inter;
        });
    })();
</script>
<!--加入购物车-->
<div class="beij_center">
    <div class="jiar_gouw_c_beij" style="position: relative">
        @if($order['status'] == '未付款')
            <img alt="模式二扫码支付" src="/org/wxpay/example/qrcode.php?data=<?php echo urlencode($url2);?>"
                 style="width:150px;height:150px;position: absolute;top:16px;left:500px;"/>
        @else
            <img src="/images/pay.png" alt="" style="width:150px;height:150px;position: absolute;top:16px;left:500px;">
        @endif


        <div class="msg"><i class="c_i_img"></i>订单生成成功！</div>
        <div class="shangp_jr">
            <div class="jr_zuo">
                <a href="#" class="jr_tu"><img src="{{$order->order_detail[0]->goods['pics'][0]??''}}"></a>
                <div class="jr_biaot">
                    <p>
                        <a href="#">{{$order->order_detail[0]->goods['title']??''}}</a>
                    </p>
                    <p class="spandf">
                        <span style="padding:0;">{{$order->order_detail[0]->product['attrs']??''}}等</span>
                        <span>数量:{{$order->order_detail[0]->num??''}}</span>
                        <span>总价:￥{{$order->total_price??''}}</span>
                    </p>
                </div>
            </div>
            <div class="jr_you">
                <a href="{{route('home.center','basic')}}" class="jr_fanh">个人中心</a>
                <a href="{{route('home.center','order')}}" class="jr_fanh">查看订单</a>
            </div>
        </div>
    </div>
</div>

{{--推荐商品--}}
<div>
    <div class="beij_center box_header">
        <h3>购买了该商品的用户还购买了</h3>
    </div>
    <div class="beij_center dgsg">
        <div class="box_body">
            <div class="item fl">
                <div class="box_img"><a href="#"><img src="{{asset('org/home/images')}}/shangq_2.jpg"></a></div>
                <div class="title">
                    <a href="#">赛妮美秋冬保暖内衣女薄款高领百搭时尚打底基础无缝美体塑身秋衣8603(精品红)</a>
                </div>
                <div class="price">¥79.00</div>
                <div class="bottom">
                    <a href="#" class="btn"><i></i><span>加入购物车</span></a>
                </div>
            </div>
            <div class="item fl">
                <div class="box_img"><a href="#"><img src="{{asset('org/home/images')}}/big_3.jpg"></a></div>
                <div class="title">
                    <a href="#">赛妮美秋冬保暖内衣女薄款高领百搭时尚打底基础无缝美体塑身秋衣8603(精品红)</a>
                </div>
                <div class="price">¥79.00</div>
                <div class="bottom">
                    <a href="#" class="btn"><i></i><span>加入购物车</span></a>
                </div>
            </div>
            <div class="item fl">
                <div class="box_img"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi3.jpg"></a></div>
                <div class="title">
                    <a href="#">赛妮美秋冬保暖内衣女薄款高领百搭时尚打底基础无缝美体塑身秋衣8603(精品红)</a>
                </div>
                <div class="price">¥79.00</div>
                <div class="bottom">
                    <a href="#" class="btn"><i></i><span>加入购物车</span></a>
                </div>
            </div>
            <div class="item fl">
                <div class="box_img"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi1.jpg"></a></div>
                <div class="title">
                    <a href="#">赛妮美秋冬保暖内衣女薄款高领百搭时尚打底基础无缝美体塑身秋衣8603(精品红)</a>
                </div>
                <div class="price">¥79.00</div>
                <div class="bottom">
                    <a href="#" class="btn"><i></i><span>加入购物车</span></a>
                </div>
            </div>
            <div class="item fl">
                <div class="box_img"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi2.jpg"></a></div>
                <div class="title">
                    <a href="#">赛妮美秋冬保暖内衣女薄款高领百搭时尚打底基础无缝美体塑身秋衣8603(精品红)</a>
                </div>
                <div class="price">¥79.00</div>
                <div class="bottom">
                    <a href="#" class="btn"><i></i><span>加入购物车</span></a>
                </div>
            </div>
            <div class="item fl">
                <div class="box_img"><a href="#"><img src="{{asset('org/home/images')}}/shangq_2.jpg"></a></div>
                <div class="title">
                    <a href="#">赛妮美秋冬保暖内衣女薄款高领百搭时尚打底基础无缝美体塑身秋衣8603(精品红)</a>
                </div>
                <div class="price">¥79.00</div>
                <div class="bottom">
                    <a href="#" class="btn"><i></i><span>加入购物车</span></a>
                </div>
            </div>
            <div class="item fl">
                <div class="box_img"><a href="#"><img src="{{asset('org/home/images')}}/big_3.jpg"></a></div>
                <div class="title">
                    <a href="#">赛妮美秋冬保暖内衣女薄款高领百搭时尚打底基础无缝美体塑身秋衣8603(精品红)</a>
                </div>
                <div class="price">¥79.00</div>
                <div class="bottom">
                    <a href="#" class="btn"><i></i><span>加入购物车</span></a>
                </div>
            </div>
            <div class="item fl">
                <div class="box_img"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi3.jpg"></a></div>
                <div class="title">
                    <a href="#">赛妮美秋冬保暖内衣女薄款高领百搭时尚打底基础无缝美体塑身秋衣8603(精品红)</a>
                </div>
                <div class="price">¥79.00</div>
                <div class="bottom">
                    <a href="#" class="btn"><i></i><span>加入购物车</span></a>
                </div>
            </div>
            <div class="item fl">
                <div class="box_img"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi1.jpg"></a></div>
                <div class="title">
                    <a href="#">赛妮美秋冬保暖内衣女薄款高领百搭时尚打底基础无缝美体塑身秋衣8603(精品红)</a>
                </div>
                <div class="price">¥79.00</div>
                <div class="bottom">
                    <a href="#" class="btn"><i></i><span>加入购物车</span></a>
                </div>
            </div>
            <div class="item fl">
                <div class="box_img"><a href="#"><img src="{{asset('org/home/images')}}/lieb_tupi2.jpg"></a></div>
                <div class="title">
                    <a href="#">赛妮美秋冬保暖内衣女薄款高领百搭时尚打底基础无缝美体塑身秋衣8603(精品红)</a>
                </div>
                <div class="price">¥79.00</div>
                <div class="bottom">
                    <a href="#" class="btn"><i></i><span>加入购物车</span></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!--底部-->
<div class="dib_beij">
    <div class="dib_jvz_beij">
        <div class="beia_hao">
            <p>京ICP备：14012449号 黔ICP证：B2-20140009号 </p>
            <p class="gonga_bei">京公网安备：11010602030054号</p>
            <div class="renz_">
                <span></span>
                <span class="span1"></span>
                <span class="span2"></span>
                <span class="span3"></span>
            </div>
        </div>
    </div>
</div>
{{--判断当前订单状态 ，如果是未付款就显示二维码让用户付款，如果已付款那么就应该把二维码改成已付款的图标--}}
{{--这里是通过定时器来查看订单状态，每隔一秒钟来查看一次，如果查到的是已付款，那么就立马把页面重新刷新一下--}}
{{--这样的话，状态就变成了已付款，那么对应的应该显示已付款的图标。--}}
@if($order['status']=='未付款')
    <script>
        $(function () {
            // 这里拿到编号是为了在后台控制器里面用编号查找对应的订单数据状态值，只是查看不是修改，修改是在微信支付成功以后
            // 已经把状态给改变了。
            var order_bianhao = "{{$order['order_bianhao']}}";
            // 设置定时器，每隔一秒去查看订单状态
            setInterval(function () {
                // $.post声明，以post方式发送异步
                /**
                 * $.post请求的四个必要参数
                 *
                 *  $.post('请求地址','请求数据',callback,'返回数据类型')
                 */
                $.post("{{route('home.checkOrderStatus')}}", {order_bianhao: order_bianhao}, function (res) {
                    if (res.code == 1) {
                        location.reload();
                    }
                }, "json");
            }, 1000);
        })
    </script>
@endif
</body>
</html>
