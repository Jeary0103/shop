@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs nav-overflow header-tabs">
                <li class="nav-item">
                    <a href="{{route('admin.category.index')}}" class="nav-link pb-3 ">
                        分类列表
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.category.create')}}" class="nav-link pb-3
                     {{active_class(if_route('admin.category.create'),'active')}}">
                        添加分类
                    </a>
                </li>
            </ul>
        </div>
        <form action="{{route('admin.category.store')}}" method="post">
            @csrf
            <div class="card-body">
                <div class="card">
                    <div class="card-header">
                        基本设置
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>分类名称</label>
                            <input type="text" name="title" class="form-control" placeholder="请输入分类名称"
                                   aria-describedby="helpId">
                        </div>
                        <div class="form-group">
                            <label for="">父级分类</label>
                            <select class="form-control" name="parent_id">
                                <option value="0">顶级分类</option>
                                @foreach($categories as $category)
                                    <option value="{{$category['id']}}">{!! $category['_title'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">分类图片</label>
                            <div class="col-12">
                                <div class="input-group ">
                                    <input class="form-control" name="icon" readonly="" value="">
                                    <div class="input-group-append">
                                        <button onclick="upImagePc(this)" class="btn btn-secondary" type="button">单图上传
                                        </button>
                                    </div>
                                </div>
                                <div style="display: inline-block;position: relative;">
                                    <img src="/images/nopic.jpg" class="img-responsive img-thumbnail" width="150">
                                    <em class="close" style="position: absolute;top: 3px;right: 8px;" title="删除这张图片"
                                        onclick="removeImg(this)">×</em>
                                </div>
                            </div>
                        </div>
                        <script>
                            require(['hdjs', 'bootstrap']);

                            //上传图片
                            function upImagePc() {
                                require(['hdjs'], function (hdjs) {
                                    var options = {
                                        multiple: false,//是否允许多图上传
                                        //data是向后台服务器提交的POST数据
                                        data: {name: '后盾人', year: 2099},
                                    };
                                    hdjs.image(function (images) {
                                        //上传成功的图片，数组类型
                                        $("[name='icon']").val(images[0]);
                                        $(".img-thumbnail").attr('src', images[0]);
                                    }, options)
                                });
                            }

                            //移除图片
                            function removeImg(obj) {
                                $(obj).prev('img').attr('src', '../dist/static/image/nopic.jpg');
                                $(obj).parent().prev().find('input').val('');
                            }
                        </script>
                        <div class="form-group">
                            <label>分类描述</label>
                            <textarea name="description" style="resize: none" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted">
                <button class="btn btn-primary btn-sm">保存提交</button>
            </div>
        </form>
    </div>
@endsection
