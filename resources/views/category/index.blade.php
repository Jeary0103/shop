@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs nav-overflow header-tabs">
                <li class="nav-item">
                    <a href="{{route('admin.category.index')}}"
                       class="nav-link pb-3  {{active_class(if_route('admin.category.index'),'active')}}">
                        分类列表
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.category.create')}}" class="nav-link pb-3">
                        添加分类
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <table class="table text-center">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>分类名称</th>
                    <th>父级分类名</th>
                    <th>创建时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category['id']}}</td>
                        <td>{{$category['title']}}</td>
                        <td>
                            @if($category['parent_id'] == 0)
                                顶级分类
                            @else
                                {!! $category->parent_name->title !!}
                            @endif
                        </td>
                        <td>
                            {{$category['created_at']->diffForHumans()}}
                        </td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                <a href="{{route('admin.category.edit',$category)}}" class="btn btn-light">编辑分类</a>
                                <button type="button" class="btn btn-white" onclick="del(this)">删除分类</button>
                                <form type="button" action="{{route('admin.category.destroy',$category)}}"
                                      method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="my--2"></div>
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                {{$categories->links()}}
            </ul>
        </nav>
    </div>
@endsection

@push('js')
    <script>
        function del(obj) {
            require(['hdjs'], function (hdjs) {
                hdjs.confirm("确定删除该分类吗？", function () {
                    $(obj).next().submit();
                })
            })
        }
    </script>
@endpush
