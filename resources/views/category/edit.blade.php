@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs nav-overflow header-tabs">
                <li class="nav-item">
                    <a href="{{route('admin.category.index')}}" class="nav-link pb-3">
                        分类列表
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.category.create')}}" class="nav-link pb-3
                     {{active_class(if_route('admin.category.edit'),'active')}}">
                        编辑分类
                    </a>
                </li>
            </ul>
        </div>
        <form action="{{route('admin.category.update',$category)}}" method="post">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="card">
                    <div class="card-header">
                        基本设置
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>分类名称</label>
                            <input type="text" name="title" value="{{$category['title']}}" class="form-control"
                                   placeholder="请输入分类名称"
                                   aria-describedby="helpId">
                        </div>
                        <div class="form-group">
                            <label for="">父级分类</label>
                            <select class="form-control" name="parent_id">
                                <option value="0">顶级分类</option>
                                @foreach($categories as $cat)
                                    <option value="{{$cat['id']}}"
                                    {{$cat['_disabled']}} {{$cat['_selected']}}
                                    >{!! $cat['_title'] !!}</option>
                                @endforeach
                            </select>
                            <small id="helpId" class="text-muted">子类和本身是不能作为父类使用的</small>
                        </div>
                        <div class="form-group">
                            <label>分类描述</label>
                            <textarea name="description" style="resize: none" class="form-control"
                                      rows="3">{{$category['description']}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted">
                <button class="btn btn-primary btn-sm">保存提交</button>
            </div>
        </form>
    </div>
@endsection
