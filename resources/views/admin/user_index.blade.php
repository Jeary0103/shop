@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs nav-overflow header-tabs">
                <li class="nav-item mt-0">
                    <a href="{{route('admin.index')}}" class="nav-link">
                        用户列表
                    </a>
                </li>
            </ul>
        </div>

        <div class="card-body small">
            <table class="table text-center">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>用户名</th>
                    <th>所属角色</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td scope="row">{{$user['id']}}</td>
                        <td>{{$user['username']}}</td>
                        {{--因为Role方法返回的是一个对象集合，也就是二维数组，所以要遍历等到某一个记录对象--}}
                        <td>
                            {{--@foreach($user->Role as $role)--}}
                                {{--<span class="badge badge-info"> {{$role['title']}} </span>--}}
                            {{--@endforeach--}}
                        </td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                <a href=" " class="btn btn-light">分配角色</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
{{--        {{$users->links()}}--}}
    </div>
@endsection

