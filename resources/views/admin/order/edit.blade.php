@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body header-body">
            <ul class="nav nav-tabs nav-overflow header-tabs">
                <li class="nav-item">
                    <a href="{{route('admin.order.index')}}"
                       class="nav-link pb-3   ">
                        订单列表
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.order.edit',$order)}}" class="nav-link pb-3 active" >
                        修改订单
                    </a>
                </li>
            </ul>
        </div>
        <form action="{{route('admin.order.update',$order)}}" method="post">
            @csrf
            @method('PUT')
            <div class="card-body">
                <table class="table text-center">
                    <thead>
                    <tr>
                        <th>订单编号</th>
                        <th>订单状态</th>
                        <th>总价</th>
                        <th>收件地址</th>
                        <th>收件人</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="height:40px; line-height: 40px;">
                        <td>{{$order['order_bianhao']}}</td>
                        <td>
                            <div >
                                <select name="status" id=""  class="form-control">
                                    <option value="未付款" {{$order['status'] == '未付款'?'selected':''}}>未付款</option>
                                    <option value="已付款"  {{$order['status'] == '已付款'?'selected':''}}>已付款</option>
                                    <option value="已发货"  {{$order['status'] == '已发货'?'selected':''}}>已发货</option>
                                    <option value="已完成"  {{$order['status'] == '已完成'?'selected':''}}>已完成</option>
                                </select>
                            </div>
                        </td>
                        <td>￥{{$order['total_price']}}</td>
                        <td>{{$order->order_address['area']}}/{{$order->order_address['address']}}</td>
                        <td>{{$order->order_address['name']}}</td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                <button  class="btn btn-light">保存修改</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
@endsection

