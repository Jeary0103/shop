@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body header-body">
            <ul class="nav nav-tabs nav-overflow header-tabs">
                <li class="nav-item">
                    <a href="{{route('admin.order.index')}}"
                       class="nav-link pb-3 active">
                        订单列表
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <table class="table text-center">
                <thead>
                <tr>
                    <th>订单编号</th>
                    <th>订单状态</th>
                    <th>总价</th>
                    <th>收件地址</th>
                    <th>收件人</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr style="height:40px; line-height: 40px;">
                        <td>{{$order['order_bianhao']}}</td>
                        <td>
                            <div >
                                <select name="status" id=""  class="form-control">
                                    <option value="" {{$order['status'] == '未付款'?'selected':''}}>未付款</option>
                                    <option value=""  {{$order['status'] == '已付款'?'selected':''}}>已付款</option>
                                    <option value=""  {{$order['status'] == '已发货'?'selected':''}}>已发货</option>
                                    <option value=""  {{$order['status'] == '已完成'?'selected':''}}>已完成</option>
                                </select>
                            </div>
                        </td>
                        <td>￥{{$order['total_price']}}</td>
                        <td>{{$order->order_address['area']}}/{{$order->order_address['address']}}</td>
                        <td>{{$order->order_address['name']}}</td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                <a href="{{route('admin.order.edit',$order)}}" class="btn btn-light">修改订单</a>
                                <button type="button" class="btn btn-white" >删除订单</button>
                                <form type="button" action=" " method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="my--2"></div>
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">

            </ul>
        </nav>
    </div>
@endsection

@push('js')
    <script>
        function del(obj) {
            require(['hdjs'], function (hdjs) {
                hdjs.confirm("确定删除该商品吗？", function () {
                    $(obj).next().submit();
                })
            })
        }
    </script>
@endpush
