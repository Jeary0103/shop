@extends('layouts.admin')
@section('content')
    <form action="{{route('admin.role.update',$role)}}" method="post">
        @csrf
        @method('PUT')
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs nav-overflow">
                    <li class="nav-item ">
                        <a href="{{route('admin.role.index')}}" class="nav-link ">
                            角色列表
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.role.edit',$role)}}" class="nav-link active">
                            编辑角色
                        </a>
                    </li>
                </ul>
            </div>
            @include('admin.layouts._role')
        </div>
    </form>
@endsection
