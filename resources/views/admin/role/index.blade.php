@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs nav-overflow">
                <li class="nav-item ">
                    <a href="{{route('admin.role.index')}}" class="nav-link active">
                        角色列表
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.role.create')}}" class="nav-link  ">
                        添加角色
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body small">
            <table class="table text-center">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>角色名称</th>
                    <th>角色标识</th>
                    <th>创建时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td scope="row">{{$role['id']}}</td>
                        <td>{{$role['title']}}</td>
                        <td>{{$role['name']}}</td>
                        <td>{{$role->created_at->format('Y-m-d')}}</td>
                        <td>

                            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                <a href="{{route('admin.role.edit',$role)}}" class="btn btn-white">编辑角色</a>
                            </div>


                                <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                    <a href=" " class="btn btn-light">分配权限</a>
                                </div>

                            {{--如果不是站长， 那么就应该可以删除的，站长是不能够删除掉的--}}

                                <button type="button" class="btn btn-white btn-sm" onclick="del(this)">删除角色</button>
                                <form action="{{route('admin.role.destroy',$role)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function del(obj) {
            require(['hdjs'], function (hdjs) {
                hdjs.confirm("确认删除角色吗?", function () {
                    $(obj).next().submit();
                })
            })
        }
    </script>
@endpush
