<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <!-- Libs CSS -->
    <link rel="stylesheet" href="{{asset('org/assets')}}/fonts/feather/feather.min.css">
    <link rel="stylesheet" href="{{asset('org/assets')}}/libs/highlight/styles/vs2015.min.css">
    <link rel="stylesheet" href="{{asset('org/assets')}}/libs/quill/dist/quill.core.css">
    <link rel="stylesheet" href="{{asset('org/assets')}}/libs/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{asset('org/assets')}}/libs/flatpickr/dist/flatpickr.min.css">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('org/assets')}}/css/theme.min.css">
    @include('layouts._hdjs')
    @include('layouts._message')
    <title>后台登录页面</title>
</head>
<body class="d-flex align-items-center bg-white border-top-2 border-primary">

<!-- CONTENT-->
<div class="container">
    <div class="row align-items-center">
        <div class="col-12 col-md-6 offset-xl-2 offset-md-1 order-md-2 mb-5 mb-md-0">
            <!-- Image -->
            <div class="text-center">
                <img src="{{asset('org/assets')}}/img/illustrations/happiness.svg" alt="..." class="img-fluid">
            </div>
        </div>
        <div class="col-12 col-md-5 col-xl-4 order-md-1 my-5">
            <h1 class="display-4 text-center mb-3">
               后台登录
            </h1>
            <!-- Form -->
            <form action="{{route('admin.login')}}" method="post">
                @csrf
                <!-- Email address -->
                <div class="form-group">
                    <label>用户名</label>
                    <input type="text" name='username' class="form-control" placeholder="请输入用户名">
                </div>
                <!-- Password -->
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label>密码</label>
                        </div>
                    </div>
                    <div class="input-group input-group-merge">
                        <input type="password" name="password" class="form-control form-control-appended" placeholder="请输入密码">
                        <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="fe fe-eye"></i>
                  </span>
                        </div>
                    </div>
                </div>
                <button class="btn btn-lg btn-block btn-primary mb-3">
                   登录
                </button>
            </form>

        </div>
    </div> <!-- / .row -->
</div> <!-- / .container -->

<!-- JAVASCRIPT-->
{{--<!-- Libs JS -->--}}
{{--<script src="{{asset('org/assets')}}/libs/jquery/dist/jquery.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/chart.js/dist/Chart.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/chart.js/Chart.extension.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/highlight/highlight.pack.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/flatpickr/dist/flatpickr.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/jquery-mask-plugin/dist/jquery.mask.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/list.js/dist/list.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/quill/dist/quill.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/dropzone/dist/min/dropzone.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/select2/dist/js/select2.min.js"></script>--}}
{{--<!-- Theme JS -->--}}
{{--<script src="{{asset('org/assets')}}/js/theme.min.js"></script>--}}

</body>
</html>
