@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs nav-overflow header-tabs">
                <li class="nav-item">
                    <a href="{{route('admin.slide.index')}}" class="nav-link pb-3 ">
                        幻灯片列表
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.slide.edit',$slide)}}" class="nav-link pb-3
                     {{active_class(if_route('admin.slide.edit'),'active')}}">
                        编辑幻灯片
                    </a>
                </li>
            </ul>
        </div>
        <form action="{{route('admin.slide.update',$slide)}}" method="post">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="card">
                    <div class="card-header">
                        基本设置
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>链接地址</label>
                            <input type="text" name="url" class="form-control" placeholder="请输入幻灯片地址"
                                   aria-describedby="helpId" value="{{$slide['url']}}">
                        </div>
                        <div class="form-group">
                            <label>幻灯片描述</label>
                            <textarea name="title" style="resize: none" class="form-control" rows="3">{{$slide['title']}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="">分类图片</label>
                            <div class="col-12">
                                <div class="input-group ">
                                    <input class="form-control" name="image" readonly="" value="{{$slide['image']}}">
                                    <div class="input-group-append">
                                        <button onclick="upImagePc(this)" class="btn btn-secondary" type="button">单图上传
                                        </button>
                                    </div>
                                </div>
                                <div style="display: inline-block;position: relative;">
                                    <img src="/images/nopic.jpg" class="img-responsive img-thumbnail" width="150">
                                    <em class="close" style="position: absolute;top: 3px;right: 8px;" title="删除这张图片"
                                        onclick="removeImg(this)">×</em>
                                </div>
                            </div>
                        </div>
                        <script>
                            require(['hdjs', 'bootstrap']);

                            //上传图片
                            function upImagePc() {
                                require(['hdjs'], function (hdjs) {
                                    hdjs.image(function (images) {
                                        //上传成功的图片，数组类型
                                        $("[name='image']").val(images[0]);
                                        $(".img-thumbnail").attr('src', images[0]);
                                    })
                                });
                            }
                            //移除图片
                            function removeImg(obj) {
                                $(obj).prev('img').attr('src', '../dist/static/image/nopic.jpg');
                                $(obj).parent().prev().find('input').val('');
                            }
                        </script>
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted">
                <button class="btn btn-primary btn-sm">保存提交</button>
            </div>
        </form>
    </div>
@endsection
