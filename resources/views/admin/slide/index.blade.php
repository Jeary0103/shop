@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs nav-overflow header-tabs">
                <li class="nav-item">
                    <a href="{{route('admin.slide.index')}}"
                       class="nav-link pb-3  {{active_class(if_route('admin.slide.index'),'active')}}">
                        幻灯片列表
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.slide.create')}}" class="nav-link pb-3">
                        新增幻灯片
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <table class="table text-center">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>描述</th>
                    <th>跳转地址</th>
                    <th>缩略图</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($slides as $slide)
                    <tr>
                        <td>{{$slide['id']}}</td>
                        <td>{{$slide['title']}}</td>
                        <td>{{$slide['url']}}</td>
                        <td>
                            <a href="{{$slide['url']}}"><img class="avatar" src="{{$slide['image']}}" alt=""></a>
                        </td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                <a href="{{route('admin.slide.edit',$slide)}}" class="btn btn-light">编辑幻灯片</a>
                                <button type="button" class="btn btn-white" onclick="del(this)">删除幻灯片</button>
                                <form type="button" action="{{route('admin.slide.destroy',$slide)}}"
                                      method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="my--2"></div>
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
{{--                {{$slides->links()}}--}}
            </ul>
        </nav>
    </div>
@endsection

@push('js')
    <script>
        function del(obj) {
            require(['hdjs'], function (hdjs) {
                hdjs.confirm("确定删除该幻灯片吗？", function () {
                    $(obj).next().submit();
                })
            })
        }
    </script>
@endpush

