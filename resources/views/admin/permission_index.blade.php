@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body">
            @foreach($modules as $module)
                <div class="card">
                    <div class="card-header font-weight-bold">
                        {{$module['name']}}
                    </div>
                    <div class="card-body">
                        <div class="row">
                            {{--循环模型里面的权限配置项数据，得到的是二维数组，进行比遍历--}}
                            @foreach($module['permission'] as $permission)
                                <div class="col-4 mb-2 mt-2">
                                    {{--把得到的一位数组的title，和name值都取出来进行展示--}}
                                    {{$permission['title']}}【{{$permission['name']}}】
                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>
            @endforeach
        </div>
    </div>
@endsection
