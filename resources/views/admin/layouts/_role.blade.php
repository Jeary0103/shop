{{--//添加和编辑是同一个模板，所以可以拿出来放到一个公共的地方，_role.blade.php--}}
<div class="card-body">
    <div class="form-group">
        <label for="">角色名称</label>
        <input type="text" name="title" class="form-control" placeholder="" value="{{old('title',$role['title']??'')}}"
               aria-describedby="helpId">
        <small id="helpId" class="text-muted">请输入中文名称</small>
    </div>
    <div class="form-group" {{active_class(if_route('admin.role.edit'),'hidden')}}>
        <label for="">角色标识</label>
        <input type="text" name="name" class="form-control" value="{{old('name',$role['name']??'')}}" placeholder=""
               aria-describedby="helpId">
        <small id="helpId" class="text-muted">请输入英文标识，只能添加不能修改</small>
    </div>
</div>
<div class="card-footer text-muted ">
    <button type="" class="btn btn-primary btn-sm">保存提交</button>
</div>

@push('css')
    <style>
        .hidden{
            display:none;
        }
    </style>
@endpush
