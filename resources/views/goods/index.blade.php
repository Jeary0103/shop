@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body header-body">
            <ul class="nav nav-tabs nav-overflow header-tabs">
                <li class="nav-item">
                    <a href="{{route('admin.goods.index')}}"
                       class="nav-link pb-3  {{active_class(if_route('admin.goods.index'),'active')}}">
                        商品列表
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.goods.create')}}" class="nav-link pb-3">
                        添加商品
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <table class="table text-center">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>商品名称</th>
                    <th>商品价格</th>
                    <th>商品图片</th>
                    <th>所属分类</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>

                @foreach($goods as $good)

                    <tr style="height:40px; line-height: 40px;">
                        <td>{{$good['id']}}</td>
                        <td>{{$good['title']}}</td>
                        <td>￥{{$good['price']}}</td>
                        <td>
                           <img src="{{$good['pics'][0]}}" class="avatar"/>
                        </td>
                        <td>
                            {{--具体商品调用它的方法，来获取它对应的分类--}}
                            {{$good->getCatId()}}
                        </td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                <a href="{{route('admin.goods.edit',$good)}}" class="btn btn-light">编辑商品</a>
                                <button type="button" class="btn btn-white" onclick="del(this)">删除商品</button>
                                <form type="button" action="{{route('admin.goods.destroy',$good)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="my--2"></div>
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                {{$goods->links()}}
            </ul>
        </nav>
    </div>
@endsection

@push('js')
    <script>
        function del(obj) {
            require(['hdjs'], function (hdjs) {
                hdjs.confirm("确定删除该商品吗？", function () {
                    $(obj).next().submit();
                })
            })
        }
    </script>
@endpush
