@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body header-body">
            <ul class="nav nav-tabs nav-overflow header-tabs">
                <li class="nav-item">
                    <a href="{{route('admin.goods.index')}}" class="nav-link pb-3 ">
                        商品列表
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.goods.create')}}" class="nav-link pb-3
                     {{active_class(if_route('admin.goods.create'),'active')}}">
                        添加商品
                    </a>
                </li>
            </ul>
        </div>
        <form action="{{route('admin.goods.store')}}" method="post">
            @csrf
            {{--商品信息列表--}}
            <div class="card-body">
                <div class="card">
                    <div class="card-header  bg-light">
                        商品设置
                    </div>
                    <div class="card-body">
                        {{--名称--}}
                        <div class="form-group">
                            <label>商品名称</label>
                            <input type="text" name="title" class="form-control" placeholder="请输入商品名称"
                                   value="{{old('title')}}">
                        </div>
                        {{--价格--}}
                        <div class="form-group">
                            <label>商品价格</label>
                            <input type="text" name="price" class="form-control" placeholder="请输入商品价格"
                                   value="{{old('price')}}">
                        </div>
                        {{--分类--}}
                        <div class="form-group pl-3 pr-3">
                            <label for="" class="ml--3">所属分类</label>
                            <div class="row">
                                {{--一级分类--}}
                                <select class="form-control col-4" name="category_id[]" id="one_cat">
                                    <option value="">请选择顶级分类</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category['id']}}">{{$category['title']}}</option>
                                    @endforeach
                                </select>
                                {{--二级分类--}}
                                <select class="form-control col-4" name="category_id[]" id="two_cat">
                                    <option value="">请选择二级分类</option>
                                </select>
                                {{--三级分类--}}
                                <select class="form-control col-4" name="category_id[]" id="three_cat">
                                    <option value="">请选择三级分类</option>
                                </select>
                            </div>
                        </div>
                        {{--图片--}}
                        <div class="form-group" style="overflow: hidden">
                            <label>商品图片</label>
                            <div class="input-group mb-1">
                                <input class="form-control" name="pics" readonly="" value="">
                                <div class="input-group-append">
                                    <button onclick="upImageMul(this)" class="btn btn-primary" type="button">选择图片
                                    </button>
                                </div>
                            </div>
                            <div id="box"></div>
                        </div>
                        {{--描述--}}
                        <div class="form-group">
                            <label>商品描述</label>
                            <textarea name="description" style="resize: none" class="form-control" rows="2"></textarea>
                        </div>
                        {{--详情--采用百度百度编辑器--}}
                        <div class="form-group">
                            <label>商品详情</label>
                            <textarea id="container" name="content" style="height:300px;width:100%;"></textarea>
                            <script>
                                require(['hdjs'], function (hdjs) {
                                    hdjs.ueditor('container', {hash: 2, data: 'hd'}, function (editor) {
                                        console.log('编辑器执行后的回调方法1')
                                    });
                                })
                            </script>
                        </div>
                        {{--是否推荐--}}
                        <div class="form-group">
                            <label for="" class="mr-2">推荐：</label>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="is_commend"
                                       class="custom-control-input"
                                       value="1">
                                <label class="custom-control-label" for="customRadioInline1">是</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline2" name="is_commend"
                                       class="custom-control-input"
                                       value="0">
                                <label class="custom-control-label" for="customRadioInline2">否</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--货品信息列表--}}
            <div class="card-body mt--5" id="app">
                <div class="card">
                    <div class="card-header bg-light">
                        货品设置
                    </div>
                    <div class="card-body">
                        <div class="card" v-for="(v,k) in products">
                            <span class="fe fe-x-circle" @click="del(k)" style="cursor: pointer; text-align:right;"
                                  title="删除"></span>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>货品属性</label>
                                    <input type="text" v-model="v.attrs" class="form-control"
                                           placeholder="可以输入多个属性，但是要用,隔开"
                                           aria-describedby="helpId">
                                </div>
                                <div class="form-group">
                                    <label>货品库存</label>
                                    <input type="text" v-model="v.kucun" class="form-control" placeholder=""
                                           aria-describedby="helpId">
                                </div>
                                <div class="form-group">
                                    <label>附加价格</label>
                                    <input type="text" v-model="v.add_price" class="form-control" placeholder=""
                                           aria-describedby="helpId">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        <button type="button" class="btn btn-light btn-sm" @click="add()">添加货品</button>
                    </div>
                </div>
                <textarea name="products" hidden class="form-control" id="" cols="30" rows="10">@{{products}}</textarea>
            </div>
            {{--提交按钮--}}
            <div class="card-footer text-muted">
                <button class="btn btn-primary btn-sm">保存提交</button>
            </div>
        </form>
    </div>
@endsection

@push('css')
    <style>
        #box img {
            width: 200px;
            float: left;
            margin-right: 10px;
            border: solid 1px #999;
            padding: 10px;
            height: 200px;
        }
    </style>
@endpush

@push('js')
    <script>
        // 无限分类异步请求
        require(['hdjs', 'jquery'], function (hdjs, $) {
            // 当顶级分类的change事件发生改变时
            $('#one_cat').change(function () {
                // 在发送异步请求之前，应该对应的二级分类的数据清空
                $('#two_cat').html("<option value=''>请选择二级分类</option>");
                // 当一级分类表单被选中时，就触发change事件，把对应的ID给拿出来
                let id = $(this).val();
                if (id) {
                    // 发送异步请求,获取当前选择的分类的子分类数据,使用category 模型的 getSonCategory方法
                    $.ajax({
                        url: '/admin/getSonCategory/' + id,// 请求地址
                        method: 'get', // 请求的方式，我们是通过路由的方式访问的，所以用get方式请求
                        dataType: 'json',// 返回来的数据格式，指的是请求返回来的数据格式
                        success: function (rel) { // 请求成功时的回调方法，rel是成功时返回的数据
                            let html = ''; //定义一个空字符串，来接收回调函数得到的每一个opion数据
                            $.each(rel, function (k, v) {  // each函数循环得到的数组
                                html += '<option value="' + v.id + '">' + v.title + '</option>';
                            });
                            $("#two_cat").append(html);
                        }
                    })
                }
            });
            $('#two_cat').change(function () {
                // 同样，先清空3级分类的数据
                $("#three_cat").html('<option value="">请选择三级分类</option>');
                // 得道二级分类的ID
                let id = $(this).val();
                if (id) {
                    $.ajax({
                        url: '/admin/getSonCategory/' + id,
                        method: 'get',
                        dataType: 'json',
                        success: function (rel) {
                            let html = '';
                            $.each(rel, function (k, v) {
                                html += '<option value="' + v.id + '">' + v.title + '</option>'
                            });
                            // 把循环得到的数据一同追加到三级栏目
                            $("#three_cat").append(html);
                        }

                    });
                }
            });
        });

        // 百度编辑器
        require(['hdjs'], function (hdjs) {
            hdjs.ueditor('container', {hash: 2, data: 'hd'}, function (editor) {
                console.log('编辑器执行后的回调方法1')
            });
        })

        //上传图片
        function upImageMul(obj) {
            require(['hdjs'], function (hdjs) {
                hdjs.image(function (images) {
                    $(images).each(function (k, v) {
                        $("<img src='" + v + "'/><input hidden name='pics[]' value='" + v + "' />").appendTo('#box');
                    })
                }, {
                    //上传多图
                    multiple: true,
                })
            });
        }

        // 添加货品代码块
        require(['hdjs', 'vue'], function (hdjs, vue) {
            new vue({
                el: "#app",
                data: {
                    products: []
                },
                methods: {
                    add() {
                        this.products.push({'attrs': '', 'kucun': '', 'add_price': ''})
                    },
                    del(k) {
                        this.products.splice(k, 1)
                    }
                }
            });
        });
    </script>
@endpush
