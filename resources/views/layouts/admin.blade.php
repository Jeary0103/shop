<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <!-- Libs CSS -->
    <link rel="stylesheet" href="{{asset('org/assets')}}/fonts/feather/feather.min.css">
    <link rel="stylesheet" href="{{asset('org/assets')}}/libs/highlight/styles/vs2015.min.css">
    <link rel="stylesheet" href="{{asset('org/assets')}}/libs/quill/dist/quill.core.css">
    <link rel="stylesheet" href="{{asset('org/assets')}}/libs/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{asset('org/assets')}}/libs/flatpickr/dist/flatpickr.min.css">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('org/assets')}}/css/theme.min.css">
    @stack('css')
    @include('layouts._hdjs')
    @include("layouts._message")
    <title>商城</title>
</head>
<body>
<!-- 左侧菜单栏-->
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white">
    <div class="container-fluid">
        <!-- 后台logo -->
        <a class="navbar-brand" href="index.html">
            <img src="{{asset('org/assets')}}/img/logo.svg" class="navbar-brand-img
          mx-auto" alt="...">
        </a>
        <!-- 竖的导航条 -->
        <div class="collapse navbar-collapse" id="sidebarCollapse">
            <!-- 侧边导航栏 -->
            <ul class="navbar-nav">
                {{--首页--}}
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.index')}}">
                        <i class="fe fe-home"></i>后台首页
                    </a>
                </li>
                {{--后台管理--}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarPages" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarPages">
                        <span class="fe fe-settings"></span> 后台管理
                    </a>
                    <div class="collapse " id="sidebarPages">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link ">
                                    模块列表 <span class="badge badge-soft-success ml-auto">modules</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.permission')}}" class="nav-link ">
                                    权限列表 <span class="badge badge-soft-success ml-auto">permission</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link ">
                                    系统配置 <span class="badge badge-soft-success ml-auto">system</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                {{--分类管理--}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarAuth2" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarAuth">
                        <span class="fe fe-grid"></span> 分类管理
                    </a>
                    <div class="collapse" id="sidebarAuth2">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{route('admin.category.index')}}" class="nav-link ">
                                    分类列表 <span class="badge badge-soft-success ml-auto">New</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.category.create')}}" class="nav-link ">
                                    添加分类 <span class="badge badge-soft-success ml-auto">New</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                {{--商品管理--}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarAuth3" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarAuth">
                        <span class="fe fe-shopping-bag"></span> 商品管理
                    </a>
                    <div class="collapse" id="sidebarAuth3">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{route('admin.goods.index')}}" class="nav-link ">
                                    商品列表 <span class="badge badge-soft-success ml-auto">list</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.goods.create')}}" class="nav-link ">
                                    添加商品<span class="badge badge-soft-success ml-auto">add</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                {{--订单管理--}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarAuthdd" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarAuth">
                        <span class="fe fe-disc "></span> 订单管理
                    </a>
                    <div class="collapse" id="sidebarAuthdd">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{route('admin.order.index')}}" class="nav-link ">
                                   订单列表 <span class="badge badge-soft-success ml-auto">list</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                {{--权限管理--}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarAuth" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarAuth">
                        <i class="fe fe-user"></i> 用户与角色
                    </a>
                    <div class="collapse" id="sidebarAuth">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{route('admin.role.index')}}" class="nav-link ">
                                    角色管理 <span class="badge badge-soft-success ml-auto">role</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.index')}}" class="nav-link ">
                                    用户管理 <span class="badge badge-soft-success ml-auto">user</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                {{--幻灯片管理--}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarAuth5" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarAuth">
                        <span class="fe fe-film"></span> 幻灯片管理
                    </a>
                    <div class="collapse" id="sidebarAuth5">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{route('admin.slide.index')}}" class="nav-link ">
                                    幻灯片列表 <span class="badge badge-soft-success ml-auto">list</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.slide.create')}}" class="nav-link ">
                                    新增幻灯片 <span class="badge badge-soft-success ml-auto">add</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <hr class="my-3">
        </div>
    </div>
</nav>

<!-- 右侧主体部分 -->
<div class="main-content">
    {{--右侧头部导航--}}
    <nav class="navbar navbar-expand-md navbar-light bg-white d-none d-md-flex">
        <div class="container-fluid">
            <a class="navbar-brand mr-auto" href="javascript:;">
                欢迎光临本商城！
            </a>
            <!-- 表单搜索部分 -->
            <form class="form-inline mr-4 d-none d-md-flex">
                <div class="input-group input-group-rounded input-group-merge" data-toggle="lists"
                     data-lists-values='["name"]'>
                    <input type="search" class="form-control form-control-prepended  dropdown-toggle search"
                           data-toggle="dropdown" placeholder="Search" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fe fe-search"></i>
                        </div>
                    </div>
                    <!-- Menu -->
                    <div class="dropdown-menu dropdown-menu-card">
                        <div class="card-body">
                            <!-- 每一个下拉菜单 -->
                            <div class="list-group list-group-flush list my--3">
                                <a href="team-overview.html" class="list-group-item px-0">
                                    <div class="row align-items-center">
                                        <div class="col-auto">

                                            <!-- Avatar -->
                                            <div class="avatar">
                                                <img src="{{asset('org/assets')}}/img/avatars/teams/team-logo-1.jpg"
                                                     alt="..." class="avatar-img rounded">
                                            </div>

                                        </div>
                                        <div class="col ml--2">

                                            <!-- Title -->
                                            <h4 class="text-body mb-1 name">
                                                Airbnb
                                            </h4>

                                            <!-- Time -->
                                            <p class="small text-muted mb-0">
                                                <span class="fe fe-clock"></span>
                                                <time datetime="2018-05-24">Updated 2hr ago</time>
                                            </p>

                                        </div>
                                    </div> <!-- / .row -->
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- 顶部右侧会员和通知部分 -->
            <div class="navbar-user">
                <!-- Dropdown -->
                <div class="dropdown mr-4 d-none d-md-flex">
                    <!-- Toggle -->
                    <a href="#" class="text-muted" role="button" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                <span class="icon active">
                  <i class="fe fe-bell"></i>
                </span>
                    </a>
                    <!-- Menu -->
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <!-- Title -->
                                    <h5 class="card-header-title">
                                        通知
                                    </h5>
                                </div>
                                <div class="col-auto">
                                    <!-- Link -->
                                    <a href="#!" class="small">
                                        View all
                                    </a>
                                </div>
                            </div> <!-- / .row -->
                        </div> <!-- / .card-header -->
                        <div class="card-body">
                            <!-- List group -->
                            <div class="list-group list-group-flush my--3">
                                <a class="list-group-item px-0" href="#!">

                                    <div class="row">
                                        <div class="col-auto">

                                            <!-- Avatar -->
                                            <div class="avatar avatar-sm">
                                                <img src="{{asset('org/assets')}}/img/avatars/profiles/avatar-1.jpg"
                                                     alt="..." class="avatar-img rounded-circle">
                                            </div>

                                        </div>
                                        <div class="col ml--2">

                                            <!-- Content -->
                                            <div class="small text-muted">
                                                <strong class="text-body">Dianna Smiley</strong> shared your post with
                                                <strong class="text-body">Ab Hadley</strong>, <strong class="text-body">Adolfo
                                                    Hess</strong>, and <strong class="text-body">3 others</strong>.
                                            </div>

                                        </div>
                                        <div class="col-auto">

                                            <small class="text-muted">
                                                2m
                                            </small>

                                        </div>
                                    </div> <!-- / .row -->

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Dropdown -->
                <div class="dropdown">
                    <!-- Toggle -->
                    <a href="#" class="avatar avatar-sm avatar-online dropdown-toggle" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{asset('org/assets')}}/img/avatars/profiles/avatar-1.jpg" alt="..."
                             class="avatar-img rounded-circle">
                    </a>
                    <!-- Menu -->
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="profile-posts.html" class="dropdown-item">Profile</a>
                        <a href="settings.html" class="dropdown-item">设置</a>
                        <hr class="dropdown-divider">
                        <a href="{{route('admin.logout')}}" class="dropdown-item">退出</a>
                    </div>

                </div>

            </div>

        </div> <!-- / .container-fluid -->
    </nav>
    <div class="container pt-2">
        @yield('content')
    </div>
</div>
<!-- JS文件 -->
{{--<script src="{{asset('org/assets')}}/libs/jquery/dist/jquery.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/chart.js/dist/Chart.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/chart.js/Chart.extension.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/highlight/highlight.pack.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/flatpickr/dist/flatpickr.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/jquery-mask-plugin/dist/jquery.mask.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/list.js/dist/list.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/quill/dist/quill.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/dropzone/dist/min/dropzone.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/libs/select2/dist/js/select2.min.js"></script>--}}
{{--<script src="{{asset('org/assets')}}/js/theme.min.js"></script>--}}
@stack('js')
</body>
</html>
