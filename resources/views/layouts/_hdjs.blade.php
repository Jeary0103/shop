<script>
    window.hdjs={};
    //组件目录必须绝对路径
    window.hdjs.base = '/org/hdjs';
    //上传文件后台地址
    window.hdjs.uploader = '{{route('common.uploader.make')}}?';
    //获取文件列表的后台地址
    window.hdjs.filesLists = '{{route('common.uploader.lists')}}?';
</script>
<script src="/org/hdjs/require.js"></script>
<script src="/org/hdjs/config.js"></script>

{{--当页面把页面上的样式都注销掉的时候，bootstrap也会被注销，所以在这里要手动引入一下--}}
<script>
    require(['bootstrap'])
</script>
