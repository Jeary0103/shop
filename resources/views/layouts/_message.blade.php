@if($errors->count()>0)
    <script>
        require(['hdjs'], function (hdjs) {
            hdjs.swal({
                text: "@foreach($errors->all() as $error) {{$error}}\n  @endforeach",
                button:false,
                icon:'info'
            });
        })
    </script>
@endif

@if(session()->has('success'))
    <script>
        require(['hdjs'], function (hdjs) {
            hdjs.swal({
                text: "{{session()->get('success')}}",
                button:false,
                icon:'success'
            });
        })
    </script>
@endif

@if(session()->has('error'))
    <script>
        require(['hdjs'], function (hdjs) {
            hdjs.swal({
                text: "{{session()->get('error')}}",
                button:false,
                icon:'error'
            });
        })
    </script>
@endif
