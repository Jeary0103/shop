<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Goods extends Model
{
    protected $guarded = ['products'];

    protected $casts = [
        // 货品表面的所属分类，是以1，2,3级连贯操作一个数组来存储所属分类的，所以这个要允许为数组
        // 图片也是，可以有多张图片，所以存储图片的地址也是以一个数组的形式存在的
        'category_id' => 'array',
        'pics' => 'array',
    ];

    // 商品表关联货品表的方法 ，一对多 用hasMany方法
    public function product()
    {
        return $this->hasMany(Product::class, 'goods_id', 'id');
    }

    // 商品与分类：
    //  获取商品对应的分类，在首页展示的时候用
    public function getCatId()
    {
        // 得到的一个集合对象 这里是用来了模型的概念，来调用自己的一个属性来获取到当前商品所在的分类
        // 但是这只是取出了分类的ID ，所以要通过ID 找到对应的分类数据从而取出分类的名称 title的值。
        $categories_id = $this->category_id;
        $categoryName = '';
        foreach ($categories_id as $category_id) {
            $categoryName .= Category::find($category_id)['title'] ."-";
        }
        return rtrim($categoryName,'-');
    }

}
