<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['order_bianhao','status','adress_id','user_id','total_price'];

    public function order_detail(){
        return $this->hasMany(Order_Detail::class,'order_id','id');
    }

    //关联地址表
    public function order_address(){
        return $this->belongsTo(Address::class,'address_id','id');
    }
}

