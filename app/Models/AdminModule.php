<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminModule extends Model
{
    protected $fillable = ['name', 'config', 'module', 'permission'];
    protected $casts = [
        'config' => 'array',
        'permission' => 'array',
    ];
}
