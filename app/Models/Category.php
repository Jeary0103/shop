<?php

namespace App\Models;

use houdunwang\arr\Arr;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['title', 'parent_id', 'icon'];

    /**
     * 重点：在分类操作时，主要看站立的角度问题，角度不同使用的方法就不同 。都是相对而言的
     * 自关联：找子类 或者 父类的方法注意事项：
     * 1. 如果是找子类，是一对多的关系
     * 2. 如果是找父类，是多对一的关系
     * 3. belongTo 和 hasMany 区别：
     * 语法结构： 都是3个参数，
     *  第一个都是被关联的对象模型，
     *  第二个参数： 如果是belongTo，则是内键，如果是hanMany则是外键
     *  第三个参数跟第二各参数恰恰相反
     */
    // ** 找父类 返回值: 所有父类数据 **
    public function parent_name()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    // 通过自关联来获取子类数据 ,找孩子是一对多的关系，所以用hasMany方法
    public function category()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }


    // ** 找子类 返回值：所有子类数据 **
    public function getSonCategory($id)
    {
        $categories = Category::where('parent_id', $id)->get();

        return $categories;
    }

    // 获取分类的树状结构图
    public function getTreeCategory()
    {
        // 首先要获取到所有的数据
        $data = Category::get()->toArray();

        return Arr::tree($data, 'title', 'id', 'parent_id');
    }

    // 获取所有的分类数据,主要在编辑的时候使用
    public function getAllCategory()
    {
        // 首先，获取分类的树状结构
        $categories = $this->getTreeCategory();
        // 确定当前编辑的数据的级别，根据级别来判断是否可用
        // 定义一个空变量，来接收增加了级别编号的当前编辑的数据
        $currentCategory = [];
        foreach ($categories as $category) {
            if ($category['id'] == $this['id']) {
                // $currentCategory 代表当前正在编辑的数据
                $currentCategory = $category;
            }
        }
        // 用循环到的数据的级别跟当前编辑的数据级别进行比较，如果大于或等于当前就不可选
        foreach ($categories as $k => $category) {
            // 大于等于当前_level不选中
            if ($category['_level'] >= $currentCategory['_level']) {
                $categories[$k]['_disabled'] = 'disabled';
            } else {
                $categories[$k]['_disabled'] = '';
            }
            // 找当前编辑的数据父级，如果找到了就默认选中selected
            if ($category['id'] == $currentCategory['parent_id']) {
                $categories[$k]['_selected'] = 'selected';
            } else {
                $categories[$k]['_selected'] = '';
            }
        }

        return $categories;
    }

    /**
     * 判断某个分类是否有子分类，返回值为真，假
     */
    public function hasChild($category)
    {
        // 调用arr组件的判断是否有子集方法,如果有,返回真,如果没有,返回假
        return Arr::hasChild($this->getAllCategory(), $category['id'], 'parent_id');
    }


    /**
     *  通过二级分类分类找三级分类下的商品的方法
     *  而且是一个分类下面找一条数据
     */
    public function getGoods()
    {
        // 首先要获取到二级分类下三级分类数据 ，找到的是一个对象集合,我们只想一页展示8个3级分类的数据，所以
        // 这里做了限制条件为8 ，而且是一个3级分类下面取一个数据
        $sonCategory = $this->category()->limit(8)->get();
        // 思路：找商品，就应该找到最低级的3级分类才可以找到分类对应的商品，所以我们这里如上是二级分类调用了getGoods方法，
        // 从而可以获取到二级分类对应下的所有三级分类，根据3级分类的ID 是否在某一个商品的category_id数组中，就可以
        // 就可以确定当前3级分类下有哪些商品，这里3级分类有多个，商品更是有多个，所以用分类去匹配商品数据，只能是
        // 每一个分类都去匹配一次所有的商品，商品要循环，分类是外部循环，所以涉及到了循环的嵌套问题。
        $goodsIds = [];
        foreach ($sonCategory as $category) { // 得到的是每一个3级分类，进行循环比较
            // 求出所有的商品数据
            $goods = Goods::orderby('id', 'DESC')->get();
            //  对所有的商品数据再次进行循环，判断外部循环的分类ID是否在某一个商品的category_id内
            foreach ($goods as $good) {
                // 如果当前分类的ID 在 商品数据的category，说明该商品在当前分类下
                // 把商品的ID拿出来，存在一个数组中，一会儿一次性取出来尽心循环
                if (in_array($category['id'], $good['category_id'])) {
                    $goodsIds[] = $good['id'];
                    break; // 当某一个分类下找到了，就跳出循环，那么这样就可以实现一个分类下找到一个数据的功能
                }
            }

        }
        // 截止这里，只是把商品的ID求出来了，而且是限制了最多8条记录
        // 接下来就是要获取这些数据了,从而就可以后取道每一个3级分类下的最新的一个商品数据了
        // 求出商品ID在循环出来的ID集合中的所有商品
        $category_goods = Goods::whereIn('id', $goodsIds)->get();

        return $category_goods;
    }

    // 获取某一个分类下面的所有的商品数据
    public function getCategoryGoods()
    {
        $goods = Goods::get();
        $goodsIds = [];
        foreach($goods as $good){
            if(in_array($this->id,$good['category_id'])){
                $goodsIds[] = $good['id'];
            }
        }
        $categoryGoods = Goods::whereIn('id',$goodsIds)->get();
        // 把得到的所有分类数据返回去
        return $categoryGoods;
    }

    // 获取当前分类的父级分类
    public function getFatherCategory(){
        $fatherCategory = Arr::parentChannel($this->getTreeCategory(), $this->id, $fieldPri = 'id', $fieldPid = 'parent_id');
        return array_reverse($fatherCategory);
    }
}
