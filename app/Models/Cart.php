<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = [];
    //在进入购物车页面时，我们想在获取购物车数据的同时，通过到购物车字段商品，和货品的ID 对应的商品和货品数据
    // 可以用with方法把商品，和货品带过去，所以这里要进行关联
    public function goods(){
        return $this->belongsTo(Goods::class,'goods_id','id');
    }
    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
