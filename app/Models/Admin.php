<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
// 当执行完权限加载5张表以后要把这段话放在用户表里，且use一下
use Spatie\Permission\Traits\HasRoles;
// 当我们使用到两个用户表的时候 设置了守卫，要记得把admin模型里的继承变成User 框架默认的。
class Admin extends User
{
    use HasRoles;

    protected $fillable = ['username','password'];
    // 为了防止后台退出登录提示remember_token报错,我们将remember_token的值设置为空
    protected $rememberTokenName = '';
}

