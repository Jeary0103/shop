<?php

namespace App\Console\Commands;

use App\Models\AdminModule;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ModuleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $message;
    protected $signature = 'hd-module';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '初始化系统模块';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->message = '命令执行成功！';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $modules = glob('app/Http/controllers/*');
        // 判断controllers下面的那个文件夹属于模块，如果是模块往下执行
        foreach ($modules as $module) {
            // 判断是当前文件夹是否为模块
            if ($this->isModule($module)) {
                // 是的话，往模块表里面添加模块设置的配置项数据，同时加入了权限配置项数据
                $this->createModule($module);
            }
        }
        $this->info($this->message); // 日志函数
    }

    // 判断当前文件夹是否为模块 ,约定俗称里面是否有config/app.php
    public function isModule(string $module): bool
    {
        return is_file($module.'/config/app.php');
    }

    /**
     * 如果是模块，就应该把模块的配置项数据添加到模块管理哪个表里面
     */
    public function createModule(string $name)
    {
        $config = include $name.'/config/app.php'; // 作为一个对象来进行表填充
        $config['config'] = $config;
        $config['module'] = basename($name);
        $config['permission'] = $this->getModulePermission($name);
        AdminModule::firstOrNew(['module'=>$config['module']])->fill($config)->save();
        return true;
    }

    //获取模块对应的权限列表
    public function getModulePermission(string $name)
    {
        $file = $name.'/config/permission.php';
        if (is_file($file)) {
            $permissions = include $file;
            foreach ($permissions as $k => $permission) {
                $permissions[$k]['name'] = basename($name).'-'.$permission['name'];
            }
            // 如果permission文件存在，那么就返回数组 ，否则的话返回一个空数组
            return $permissions;
        }
        return [];
    }
}
