<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CodeNotification extends Notification
{
    use Queueable;
    // 定义一个属性存放登录进来的用户
    protected $user;
    // 修改类中的构造方法
    public function __construct($user=null)
    {

        // 把登录进来的用户名存放到自定义类的属性里面
        $this->user = $user;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        //产生一个随机4位数，生成一个从1-9随机数的数组，从数组里面随机取4个数字，把数组转为字符串就可以生成一个4位数字
        $code = implode(array_rand(range(1,9),4));
        // 把验证码和传递过来的邮箱一起存到session里面
        session(['register' => ['code' => $code, 'email' => $this->user['email']]]);
        return (new MailMessage)
            ->line('感谢登录0349edu,您的验证码是：'.$code)
            ->action('来新邮件了', url('/'));
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
