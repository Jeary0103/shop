<?php

namespace App\Http\Controllers\Common;

use App\Models\Upload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UploaderController extends Controller
{
    public function make(Request $request,Upload $upload){
        $dir = 'upload/' . date('ym');
        $fileName = auth()->id().'-'.str_random(10) . time() . '.' . $request->file->getClientOriginalExtension();
        $file = $request->file->move($dir, $fileName);

        //往上传数据表中记录文件
        $upload['user_id'] = auth('admin')->id();
        $upload['path'] =url( $file);
        $upload['url'] = url($file);
        $upload['name'] = $request->file->getClientOriginalName();
        $upload->save();

        return ['file' => url($file), 'code' => 0];
    }
    public function lists()
    {
        $uploads = Upload::paginate(20);
        return [
            'data' => $uploads->toArray()['data'],
            'page' => $uploads->links() .'',
            'code' => 0,
        ];
    }


    public function layuiUpload(Request $request,Upload $upload){

        $dir = 'upload/' . date('ym');
        $fileName = auth()->id().'-'.str_random(10) . time() . '.' . $request->file->getClientOriginalExtension();
        $file = $request->file->move($dir, $fileName);

        //往上传数据表中记录文件
        $upload['user_id'] = auth()->id();
        $upload['path'] =url( $file);
        $upload['url'] = url($file);
        $upload['name'] = $request->file->getClientOriginalName();
        $upload->save();

        return ['code' => 0, 'msg' => '','data'=>[
            'src'=>url($file),
        ]];
    }
}
