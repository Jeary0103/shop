<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\DatabaseNotification;
use PhpParser\Comment;

class NotificationController extends Controller
{
    //阅读通知
    public function read(DatabaseNotification $notification)
    {
        $notification->markAsRead();
        $comment = Comment::find($notification->data['comment_id']);
        return redirect($comment->relationModel->link(['comment_id' => $comment['id']]));

//        $notification = auth()->user()->notifications->where('id',$id)->first();
//        dd($notification);
    }
}
