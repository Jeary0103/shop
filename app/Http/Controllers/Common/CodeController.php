<?php

namespace App\Http\Controllers\Common;

use App\Notifications\CodeNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CodeController extends Controller
{
    //向指定邮箱发送验证码，不论用户在网站是否已经存在 ,
    // 这里$user伪造了一个假的用户，去发送邮件
    public function send(Request $request, User $user)
    {
        $user->fill(['email' => request()->input('username')]);

        return $this->sendCode($user);
    }

    //已经存在的用户发
    public function sendToUser()
    {
        $user = User::where('email', request()->input('username'))->first();
        if (!$user) {
            return ['code' => 203, 'message' => '用户不存在'];
        }
        return $this->sendCode($user);
    }

    protected function sendCode($user)
    {
        $user->notify(new CodeNotification($user));
        return ['code' => 0, 'message' => '验证码发送成功'];
    }
}
