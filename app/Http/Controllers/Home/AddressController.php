<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    // 保存方法
    public function store(Request $request)
    {
        $request['user_id'] = auth()->id();
        if ($request['is_default']) {
            Address::where('user_id', auth()->id())->update(['is_default' => 0]);
        }
        Address::create($request->all());

        return back()->with('success', '添加成功！');
    }

//    更新方法
    public function update(Request $request, Address $address)
    {
        if (isset($request['is_default'])) {
            Address::where('user_id', auth()->id())->update(['is_default' => 0]);
        } else {
            $request['is_default'] = 0;
        }
        $address->update($request->all());

        return back()->with('success', '编辑成功！');
    }

//    设置默认方法
    public function destroy(Address $address)
    {
        $address->delete();
        return back()->with('success', '删除成功！');
    }

    public function setDefault(Address $address)
    {
        Address::where('user_id', auth()->id())->update(['is_default' => 0]);
        $address->update(['is_default' => 1]);
        return back()->with('success', '设置成功！');
    }
}
