<?php

namespace App\Http\Controllers\Home;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Goods;
use App\Models\Product;
use App\Models\Slide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\DocBlock\Tags\Covers;

class HomeController extends CommonController
{
    public function index(){
        // 获取轮播图数据
        $slides = Slide::latest()->limit(3)->get();
        // 获取当前用户的购物车数据，只要获取到购物车数据用with方法就可以把关联的商品，和货品一同带过去
        $carts =Cart::where('user_id',auth()->id())->latest()->with('goods','product')->get();
        // 获取顶级分类数据，目的是为了循环每一个一级分类（首页大块内容部分：如手机、电脑等）
        /**
         * 获取每分类数据，及其二级分类下的所有三级分类数据
         */
        $categories = Category::where('parent_id',0)->get();
        // 特色商品随机抽取4个分配到页面,在页面底部左侧部分 特色商品部分
        $goods = Goods::inRandomOrder()->limit(4)->get()->toArray();

        // 热门推荐商品，利用表里面的推荐字段，可以用where条件获取到推荐商品，取出随机5条
        $commend_goods = Goods::where('is_commend',1)->inRandomOrder()->limit(5)->get();

        return view('home.index',compact('categories','carts','slides','goods','commend_goods'));
    }

    // 列表页
    public function lists(Category $category){
        $carts =Cart::where('user_id',auth()->id())->latest()->with('goods','product')->get();
        $fatherCategory = $category->getFatherCategory();
        // 找当前分类下的所有商品数据
       $goods = $category->getCategoryGoods();
        return view('home.lists',compact('category','goods','fatherCategory','carts'));
    }

    // 详情页
    public function content(Goods $good,$product=0){
        $carts =Cart::where('user_id',auth()->id())->latest()->with('goods','product')->get();
        // 为了在详情页展示商品的所在分类，那么就应该先求出每一个商品所在的三级分类，然后再求出该3级分类的父级分类
        // 就可以实现分类，这样目的是为了获取分类并且在点击链接时，能够跳转到对应的分类所在的列表页
        // 找当前商品的三级分类 ,，找到最底层的分类，就可以找它的父级分类了
        $category = Category::find($good['category_id'][2]);
        $fatherCategory = array_reverse($category->getFatherCategory());
        // 判断是否传递了货品ID ，这样设计是为了实现页面的跳转来选择不同的货品属性
        if($product){
            $huopin = Product::find($product);

        }else{
            // 默认选中商品的第一个货品属性
            $huopin = Product::where('goods_id',$good['id'])->first();
        }
        return view('home.content',compact('good','huopin','fatherCategory','carts'));
    }

}
