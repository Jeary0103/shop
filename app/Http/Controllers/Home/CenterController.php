<?php

namespace App\Http\Controllers\Home;

use App\Models\Address;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Order_Detail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CenterController extends Controller
{
    // 个人中心首页加载方法
    public function index($bianliang,User $user){
        // 获取当前用户信息
        $user = User::where('id',auth()->id())->first()->toArray();
        // 获取当前用户的地址信息
        $addresses = Address::where('user_id',auth()->id())->orderby('is_default','DESC')->get();
        // 获取购物车数据的
        $carts = Cart::where('user_id',auth()->id())->with('goods','product')->latest()->limit(2)->get();
        // 获取订单表中的数据
        $orders = Order::where('user_id',auth()->id())->latest()->get();

        return view('home.center.'.$bianliang,compact('user','addresses','carts','orders'));
    }
    // 个人中心：异步删除订单表数据的方法
    public function delOrder($order_id){
       $order = Order::where('id',$order_id);
       $order->delete();
       return ['code'=>1,'msg'=>'删除成功！'];
    }
}
