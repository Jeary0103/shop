<?php

namespace App\Http\Controllers\Home;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


require_once  "./org/wxpay/example/WxPay.NativePay.php";

class PayController extends Controller
{
    // 提交订单跳转到的支付页面方法
    public function index($order_bianhao, Order $order)
    {
        // 通过路由参数获取到当前订单的编号，用依赖注入的对象调用它的where方法，就可以获取到对应的订单数据
        $order = $order->where('order_bianhao', $order_bianhao)->first();

        //微信支付二维码生成 ，逻辑代码
        $input = new \WxPayUnifiedOrder();
        $input->SetBody("王志远商城订单支付");
        $input->SetAttach($order_bianhao);
        $input->SetOut_trade_no("sdkphp123456789".date("YmdHis"));
        $input->SetTotal_fee("1");
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag("test");
        // $input->SetNotify_url(""); 成功回调函数
        $input->SetNotify_url(route('home.wxPayResult'));
        $input->SetTrade_type("NATIVE");
        $input->SetProduct_id("123456789");

        $notify = new \NativePay();
        $result = $notify->GetPayUrl($input);
        //dd($result);
        $url2 = $result["code_url"];

        return view('home.pay.pay', compact('order', 'url2'));
    }
    //微信异步通知回调地址,注意这个方法需要支付成功之后让微信通知
    //设置csrf白名单,将wxPayResult路由排除(不需要csrf令牌验证)
    public function wxPayResult()
    {
        //这句测试微信支付成功之后是否正常运行
        //file_put_contents('a.php',1);
        //接受微信推送给我们数据
        $content = file_get_contents('php://input');
        // simplexml_load_string() 函数转换形式良好的 XML 字符串为 SimpleXMLElement 对象。
        $res = simplexml_load_string($content, 'simpleXmlElement', LIBXML_NOCDATA);
        // 将数据写入到文件中，查看
        file_put_contents('a.php', var_export($res, true));
        // 判断支付成功
        if ($res->result_code == 'SUCCESS' && $res->return_code == 'SUCCESS') {
            file_put_contents('b.php',2);
            // 修改我们自己数据表中的订单状态
            Order::where('order_bianhao', $res->attach)->update(['status'=>'已付款']);
            //返回给微信数据xml数据包,告诉微信不需要再通知我了
            $xml = '<xml>
                       <return_code><![CDATA[SUCCESS]]></return_code>
                       <return_msg><![CDATA[OK]]></return_msg>
                    </xml>';
            echo $xml;
            return true;
        }
    }

    //异步检查订单是否已支付
    public function checkOrderStatus(Request $request){
        $order_bianhao = $request->order_bianhao;
        $orders = Order::where('order_bianhao',$order_bianhao)->first();
        if($orders['status']!='未付款'){
            // 说明已经付款
            return ['code'=>1,'msg'=>'已付款'];
        }else{
            return ['code'=>0,'msg'=>'未付款'];
        }
    }
}
