<?php

namespace App\Http\Controllers\Home;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function changPassword(Request $request,User $user){
        $this->validate(
            $request,
            [
                'newPassword' => 'required|confirmed',
            ],
            [
                'newPassword.required' => '密码不能为空',
                'newPassword.confirmed' => '两次密码不一致',// 确认两次输入的密码一致才可以登录
            ]
        );
       $user =  User::where('password',auth()->user()->password)->first();
       $user->update(['password'=>bcrypt($request['newPassword'])]);

       return redirect(route('home.center','index'))->with('success','密码修改成功！');
    }

    // 修改个人信息的方法
    public function gerenXinxi(Request $request){

        User::where('id',auth()->id())->update($request->except('_token','_method','file'));
        return back()->with('success','保存成功');
    }
}
