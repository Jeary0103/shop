<?php

namespace App\Http\Controllers\Home;

use App\Models\Goods;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class CommonController extends Controller
{
    public function __construct()
    {
        // 在商品表中随机去除4条商品，其他控制器通过继承该控制器就会优先加载到这些数据，所以这个很方便的
        // 会把一些公共的数据带到公共的页面上。只要继承了这个控制器就可以了。
        $goods = Goods::inRandomOrder()->limit(4)->get();
        View::share('share_goods', $goods);
    }
}
