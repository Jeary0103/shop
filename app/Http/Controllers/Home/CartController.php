<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Goods;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    // 这里因为路由加了3个参数，所以这里也加入3个参数，分别表示： 商品-货品-数量
    // 因为这里涉及到了大量的运算，所以我们选择了vue来布局控制数量的变化
    public function addCart(Goods $good, Product $product, $num)
    {
        // 首先应该去表里面找，当前用户是否已经有加入购物车的数据
        $cart = Cart::where('user_id', auth()->id())->where('product_id', $product['id'])->first();
        // 如果已经有当前货品数据，就应该在原有的基础上进行修改，而不再是添加数据
        if ($cart) {
            $cart->update(
                [
                    'num' => $num + $cart['num'],
                    'xiaoji' => ($good['price'] + $product['add_price']) * ($num + $cart['num']),
                ]
            );
        } else {
            Cart::create(
                [
                    'goods_id' => $good['id'],
                    'product_id' => $product['id'],
                    'user_id' => auth()->id(),
                    'num' => $num,
                    'xiaoji' => ($good['price'] + $product['add_price']) * $num,
                ]
            );
        }
        return back()->with('success', '添加购物车成功！');
    }
    public function quickBuy(Request $request )
    {
        $good = Goods::where('id',$request->good)->first();
        $product = Product::where('id',$request->huopin)->first();
        // 首先应该去表里面找，当前用户是否已经有加入购物车的数据
        $cart = Cart::where('user_id', auth()->id())->where('product_id',$request->huopin)->first();
        // 如果已经有当前货品数据，就应该在原有的基础上进行修改，而不再是添加数据
        if ($cart) {
            $cart->update(
                [
                    'num' => $request->num,
                    'xiaoji' => $good['price'] * ($request->num),
                ]
            );
        } else {
            Cart::create(
                [
                    'goods_id' => $request->good,
                    'product_id' => $request->huopin,
                    'user_id' => auth()->id(),
                    'num' => $request->num,
                    'xiaoji' => $good['price'] * $request->num,
                ]
            );
        }
        $cart = Cart::where('product_id',$request->huopin)->first();
        return ['code'=>1,'msg'=>'添加购物车成功！','ids'=>$cart['id']];
    }

    // 我的购物车
    public function myCart()
    {
        // 把所有的商品和货品信息一起带过去，方便分配
        $carts = Cart::where('user_id', auth()->id())->with('goods', 'product')->get();
        // 购物车是多条数据，一个对象集合，所以要进行循环
        foreach($carts as $k=>$cart){
            // 给每一个购物车数据加一个附加属性，checked，默认为不选中状态
            $carts[$k]['checked'] = false;
        }
        return view('home.myCart.cart', compact('carts'));
    }

     //异步请求：删除购物车数据的方法
    public function delete(Cart $cart){
        $cart->delete();
        return ['valid'=>'1','message'=>'删除成功'];
    }

    // 异步请求增加购物车数量增加的方法
    public function plus(Cart $cart){
        $cart->update([
            'num'=> $cart['num']+1,
            'xiaoji'=>$cart->goods->price + $cart->product->add_price + $cart['xiaoji']
        ]);
        return ['valid'=>'1','message'=>''];
    }

    // 异步请求减少购物车数量数据的方法
    public function minus(Cart $cart){
            $cart->update([
                'num'=>$cart['num']-1,
                'xiaoji'=> $cart['xiaoji'] - $cart->goods->price - $cart->product->add_price
            ]);
        return ['valid'=>'1','message'=>''];
    }
}
