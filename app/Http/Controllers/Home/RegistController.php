<?php

namespace App\Http\Controllers\Home;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegistController extends Controller
{
    public function regist()
    {

        return view('home.register.regist');
    }

    public function store(Request $request)
    {
        // 对注册表单的数进行验证
        $this->validate(
            $request,
            [
                'email' => 'email|unique:users',
                'password' => 'required|confirmed',
                'code' => 'required',
            ],
            [
                'email.email' => '邮箱错误',
                'email.unique' => '邮箱已经存在',
                'password.required' => '密码不能为空',
                'password.confirmed' => '两次密码不一致',// 确认两次输入的密码一致才可以登录
                'code.required' => '验证码不能为空',
            ]
        );
        //验证码的比较
        $status = session('register.email') == $request->input('email') &&
            session('register.code') == $request->input('code');
        if (!$status) {
            return back()->with('error', '验证码输入错误');
        }
        $request['password'] = bcrypt($request->password);
        User::create($request->all());
        return redirect(route('home.login'))->with('success', '注册成功,请登录！');
    }

}
