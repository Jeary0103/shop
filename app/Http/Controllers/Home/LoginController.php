<?php

namespace App\Http\Controllers\Home;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function __construct()
    {
         $this->middleware('home.checkLogin')->except(['logout']);
    }

    public function loginForm()
    {
        return view('home.login.login');
    }

    //登录验证方法
    public function login(Request $request)
    {
        $status = \Auth::attempt(
            [
                'email' => $request['email'],
                'password' => $request['password'],
            ]
        );
        if ($status) {
            return redirect(route('home.index'))->with('success', '登录成功！');
        }
        return back()->with('error', '账号或密码错误');
    }

    // 退出登录
    public function logout()
    {
        auth()->logout();
        return redirect(route('home.index'))->with('success', '退出成功');
    }
}
