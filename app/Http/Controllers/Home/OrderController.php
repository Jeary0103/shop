<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Order_Detail;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function index(Order $order)
    {

    }

    public function create()
    {

    }

    public function store(Request $request, Order $order)
    {
        //获取post异步请求发送过来的数据，仍然是购物车的数据编号
        $ids = $request->ids;
        // 把字符串拆分成一个数组
        $arr_ids = explode(',', $ids);
        //用购物车模型去找id在传过来id数组中的数据 ，就可以获取所有的购物车数据，用sum方法，把所有对象集合的某一个字段值
        // 的和求出来，就可以得到所有小计的和 ，就是所有商品的总价钱了。用zongjia来表示
        $zongjia = Cart::whereIn('id', $arr_ids)->get()->sum('xiaoji');

        // 因为是往订单表里面插入数据，所以要实例化一个模型对象
        $order->address_id = $request['address_id'];
        $order->user_id = auth()->id();
        $order->total_price = $zongjia;
        $order->order_bianhao = time().auth()->id();
        $order->save();
//        dd($order);

        // 往订单明细表添加对应的数据，因为是订单明细存放着商品，和货品信息所以我们这里要循环购物车数据，通过关联
        // 获取到对应的商品和货品信息，存在订单明细表里面
        $cartData = Cart::whereIn('id', $arr_ids)->get();
        foreach ($cartData as $v) {
            // 实例化一个订单明细对象，修改对象字段，保存新字段值
            $order_detail = new Order_Detail();
            $order_detail->order_id = $order->id;
            $order_detail->goods_id = $v->goods_id;
            $order_detail->product_id = $v->product_id;
            $order_detail->num = $v->num;
//            dd($order_detail);
            $order_detail->save();
        }

        //同时清空购物车数据
        $carts = Cart::whereIn('id',$arr_ids)->get();
        foreach($carts as $cart){
            $cart->delete();
        };

        // 异步要求返回
        return [
            'code' => 1,
            'message' => '订单生成成功',
            // 注意，我们这里异步请求完成以后不光要返回一个状态码，还要返回订单编号，因为我们要进行页面跳转，需要把
            // 订单编号给传递过去，所以这里我们采用了data属性来给前台返回一条数据里面就包含了订单编号这个变量
            'data' => [
                'order_bianhao' => $order->order_bianhao,
            ],
        ];
    }

    public function show(Order $order)
    {
        //
    }

    public function edit(Order $order)
    {
        //
    }

    public function update(Request $request, Order $order)
    {
        //
    }


    public function destroy(Request $request,Order $order)
    {

    }

    // 展示订单页面的方法
    public function orders(Request $request)
    {
        // 获取当前登录用户的收获地址
        $addresses = Address::where('user_id', auth()->id())->get();
        // 获取默认被选中的地址
        $defaultAddress = Address::where('user_id', auth()->id())->where('is_default', '1')->first()->toArray();
        // 获取从get参数传递过来的购物车ID ，把它转成一个数组
        $str_ids = rtrim($request->query('ids'), ',');
        $ids = explode(',', $str_ids);
        // 按照get传递过来的参数，从购物车表里面找到对应的数据
        $carts = Cart::whereIn('id', $ids)->get();
        // 计算购物车商品的总价
        $totalPrice = Cart::whereIn('id', $ids)->sum('xiaoji');
        return view(
            'home.order.order',
            compact('addresses', 'str_ids', 'ids', 'carts', 'totalPrice', 'defaultAddress')
        );
    }
}
