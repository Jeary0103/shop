<?php

namespace App\Http\Controllers\Admin;

use App\Models\AdminModule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{

    public function index()
    {
        $modules = AdminModule::get();

        return view('admin.permission_index', compact('modules'));
    }
}
