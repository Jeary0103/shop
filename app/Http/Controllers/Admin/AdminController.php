<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index(Admin $user)
    {
        $users = Admin::where('id','<>',1)->get();
        return view('admin.user_index',compact('users','user'));
    }

}
