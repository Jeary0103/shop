<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    // 展示所有角色数据
    public function index()
    {

        $roles = Role::get();
        return view('admin.role.index',compact('roles'));
    }

    public function create()
    {
        return view('admin.role.create');
    }

    public function store(Request $request)
    {
       Role::create($request->all());
       return redirect(route('admin.role.index'))->with('success','添加成功！');
    }

    public function show($id)
    {
        //
    }


    public function edit(Role $role)
    {
       return view('admin.role.edit',compact('role'));
    }

    public function update(Request $request,Role $role)
    {
       $role->update($request->all());
       return redirect(route('admin.role.index'))->with('success','编辑成功！');
    }


    public function destroy(Role $role)
    {
        // 非站长才可以删除的
        if(!$role['system']){
            $role->delete();
        }
        return redirect(route('admin.role.index'))->with('success','删除成功！');
    }
}
