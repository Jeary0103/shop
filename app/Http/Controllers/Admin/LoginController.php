<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class LoginController extends Controller
{
    // 加载后台登录模板的方法
    public function loginForm()
    {
        return view('admin.login');
    }

    // 处理后台登录表单提交数据的方法
    public function login(Request $request)
    {
        // 默认前台守卫是 'guard' => 'web',而我们现在处理后台登录用户信息的，所以不用前台注册用户的信息
        // 所谓的守卫，最终指向的还是一个表模型，验证的数据通过交给守卫来处理，也就是提交过来的数据去守卫指向
        // 的表里面去找，如果找到了，就可以往下执行，如果找不到就不能执行。这是后台管理员进入后台的必经之路！
        // 这里就是，用户提交过来的表单数据，守卫admin指向的模型也就是admins表里面找匹配的数据
        $status = Auth::guard('admin')->attempt(
            [
                'username' => $request->get('username'),
                // laravel会自动散列化提交过来的密码，所以我们这里是不需要进行加密处理的
                'password' => $request->get('password'),
            ]
        );
        // 如果提交过来的数据数据在admins表里面匹配到了，那么可以重定向到指定的页面了。但是重定向的时候，有路由的
        // 的跳转，这时候，还得通过中间件那一关，如果中间件的验证通过了才可以正常登陆。
        if ($status) {
            // 重定向页面
            return redirect(route('admin.index'))->with('success', '登录成功！');
        } else {
            return back()->with('error', '用户名或密码错误！');
        }
    }

    // 退出登录的方法
    public function logout(){
        auth('admin')->logout();
        return redirect(route('admin.login'))->with('success','退出成功！');
    }
}
