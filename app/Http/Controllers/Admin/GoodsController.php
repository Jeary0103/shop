<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GoodsRequest;
use App\Models\Category;
use App\Models\Goods;
use App\Models\Product;
use Illuminate\Http\Request;

class GoodsController extends Controller
{
    public function index()
    {
        $goods = Goods::paginate(10);
        return view('goods.index',compact('goods'));
    }

    public function create()
    {
        // 获取所有顶级分类
        $categories = Category::where('parent_id', 0)->get();
        return view('goods.create',compact('categories'));
    }

    public function store(GoodsRequest $request)
    {
        // 首先要获取到所有的数据对商品表进行填充
        $goods = Goods::create($request->all());
        // 使用input方法可以获取到货品表里面的字段值，此时为json数据，要转为数组才可以放到表里面
        $products = json_decode($request->input('products'),true);
        $goods->product()->createMany($products);
        return redirect(route('admin.goods.index'))->with('success','添加成功');
    }
    public function show(Goods $goods)
    {
        //
    }

    public function edit(Request $request,Goods $good)
    {
        // 找到顶级分类的所有数据，如果当前编辑的数据ID在编辑的category_id数组中，那么就默认选中
        $categories = Category::where('parent_id',0)->get();
        // 找二级分类
        $twoCategories = Category::where('parent_id',$good['category_id'][0])->get();
        // 找三级分类
        $threeCategories = Category::where('parent_id',$good['category_id'][1])->get();
        // 获评当前商品对应的货品属性值集合
        $products = $good->product()->get();
        return view('goods.edit',compact('categories','good','twoCategories','threeCategories','products'));
    }


    public function update(GoodsRequest $request, Goods $good)
    {
        // 首先更新商品表里面的数据
        $good->update($request->all());
        // 把商品表里面带过来的货品数据，转为数组格式的数据
        $products = json_decode($request->input('products'),true);
        // 将当前商品的对应货品全部删除，然后再将新来的添加进去
        $good->product()->delete();
        // 使用模型关联进行数据填充，关联字段不需要手动去添加的，系统会自动加上那个缺少的字段
        $good->product()->createMany($products);
        return  redirect(route('admin.goods.index'))->with('success','修改成功！');
    }

    public function destroy(Goods $good)
    {
        // 当删除商品时，因为我们在货品表里面的数据迁移文件，做了外键约束这个功能，所以在删除商品时，货品会自动被删除
        // 所以，在这里删除商品数据时，货品表里面的数据会自动被删除，不需要我们手动去删除货品表里面的数据了。。。。。
        $good->delete();
        return redirect(route('admin.goods.index'))->with('success','删除成功！');
    }
}
