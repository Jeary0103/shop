<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slide;
use Illuminate\Http\Request;

class SlideController extends Controller
{

    public function index()
    {
        $slides = Slide::get();
        return view('admin.slide.index',compact('slides'));
    }


    public function create()
    {
        return view('admin.slide.create');
    }


    public function store(Request $request)
    {
        Slide::create($request->all());
        return back()->with('success','添加成功！');
    }

    public function show(Slide $slide)
    {
        //
    }

    public function edit(Slide $slide)
    {
        return view('admin.slide.edit',compact('slide'));
    }

    public function update(Request $request, Slide $slide)
    {
        $slide->update($request->all());
        return redirect(route('admin.slide.index'))->with('success','编辑成功！');
    }


    public function destroy(Slide $slide)
    {
        $slide->delete();
        return redirect(route('admin.slide.index'))->with('success','删除成功!');
    }
}
