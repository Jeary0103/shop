<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        // 因为在列表页数据很多，所以这里用分页的方法来获取数据
        $categories = Category::paginate(10);
        return view('category.index', compact('categories'));
    }

    public function create(Category $category)
    {
        // 因为在添加是，要选择是哪个分类下的数据，所以要获取所有分类数据，用下拉框表单来遍历数据
        $categories = $category->getTreeCategory();
        return view('category.create', compact('categories'));
    }

    public function store(Request $request)
    {
        // 因为每次添加的是一条数据，所以这里用Crete方法就可以添加数据了
        Category::create($request->all());
        return redirect(route('admin.category.index'))->with('success', '添加成功');
    }

    public function show(Category $category)
    {
        //
    }

    public function edit(Category $category)
    {
        // 因为是编辑单条数据，所以这里用依赖注入的形式，获取到当前被编辑的单条数据对象进行处理
        $categories = $category->getAllCategory();
        return view('category.edit', compact('category', 'categories'));
    }

    public function update(Request $request, Category $category)
    {
        $category->update($request->all());
        return redirect(route('admin.category.index'))->with('success', '修改成功');
    }

    public function destroy(Category $category)
    {
        // 检测当前需要删除的分类是否有子分类,如果有,不让删除
        if ($category->hasChild($category)) {
            return back()->with('error', '请先删除子分类再来 (⊙﹏⊙)');
        }
        $category->delete();
        return back()->with('success', '删除成功！');
    }

    /**
     * 获取某个分类的子分类的方法
     */
    public function getSonCategory($id){
        $categories = Category::where('parent_id',$id)->get();
        return $categories;
    }
}
