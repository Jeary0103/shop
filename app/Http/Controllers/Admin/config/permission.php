<?php
return [
    ['title'=>'后台管理','name'=>'index'],
    ['title'=>'权限列表','name'=>'permission'],
    ['title'=>'系统配置','name'=>'config'],
    ['title'=>'分类管理','name'=>'category'],
    ['title'=>'商品管理','name'=>'goods'],
    ['title'=>'订单管理','name'=>'order'],
    ['title'=>'幻灯片管理','name'=>'slide'],
];
