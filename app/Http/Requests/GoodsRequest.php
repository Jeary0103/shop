<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GoodsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'price' => 'required',
            'category_id' => 'required',
            'pics' => 'required',
            'description' => 'required',
            'content' => 'required',
            // $attr 代表的是被验证的字段名称
            // $value 代表的是被验证的字段的值
            // $fail 自动获取到的错误信息对象
            // function ($attr, $value, $fail){ ... }
            'category_id' => function ($attr, $value, $fail) {
                // 如果$value中存在一个null,就代表有分类没有选择,就应该提示错误并返回
                if (in_array(null, $value)) {
                    return $fail('请选择商品分类');
                }
            },
        ];
    }

    public function message()
    {
        return [
            'title.required' => '商品标题不能为空',
            'price.required' => '商品价格不能为空',
            'description.required' => '商品描述不能为空',
            'content.required' => '商品详情不能为空',
            'pics.required' => '商品图片不能为空',
        ];

    }
}
