<?php

namespace App\Http\Middleware;

use Closure;

class checkLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()){
            return redirect(route('home.index'))->with('error','该用户已经登录！');
        }
        return $next($request);
    }
}
