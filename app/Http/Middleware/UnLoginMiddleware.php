<?php

namespace App\Http\Middleware;

use Closure;

class UnLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->guest()){
            return redirect(route('home.login'))->with('error','请登录以后再来！');
        }
        return $next($request);
    }
}
