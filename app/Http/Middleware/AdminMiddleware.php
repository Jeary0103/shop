<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AdminMiddleware
{
    /**
     * 中间件，是处理http请求的，如果http请求的是
     */
    public function handle($request, Closure $next)
    {
        // 守卫对后台登录人员进行验证，如果没有登录，那么请离开，请登录再来
        if(!Auth::guard('admin')->check()){
            return redirect()->route('admin.login')->with('error','请先登录再来');
        }
        return $next($request);
    }
}
