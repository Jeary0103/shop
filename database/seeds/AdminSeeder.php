<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * 后台管理员数据
     */
    public function run()
    {
        $admins = [
            ['username' => 'wangzhiyuan', 'password' => bcrypt('admin888')],
            ['username' => 'liqiaomei', 'password' => bcrypt('admin888')],
        ];
        foreach ($admins as $admin) {
            \App\Models\Admin::create($admin);
        }
    }
}
