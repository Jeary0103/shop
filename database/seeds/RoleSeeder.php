<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['title'=>'站长','name'=>'website','system'=>true],
            ['title'=>'管理员','name'=>'admin']
        ];
        // 填充数据只能是一个一维数组，所以要循环得到每一个一维数组才可以添加到表里面
        foreach($roles as $role){
            \Spatie\Permission\Models\Role::create($role);
        }
    }
}
