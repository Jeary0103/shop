<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name' => 'wangzhiyuan', 'email' => '237618121@qq.com', 'password' => bcrypt('admin888')],
            ['name' => 'liqiaomei', 'email' => '369221664@qq.com', 'password' => bcrypt('admin888')],
        ];
        foreach ($users as $user) {
            \App\User::create($user);
        }
    }
}
