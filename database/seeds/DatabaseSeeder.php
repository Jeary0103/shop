<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $this->call(RoleSeeder::class);  //先有角色，才能分配角色，所以要放在用户之前
        $this->call(AdminSeeder::class); // 后台管理员数据填充
        $this->call(UserSeeder::class); // 前台用户数据填充
    }
}
