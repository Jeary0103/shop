<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('模块名称');
            $table->string('module')->comment('模块所在目录文件夹名称,也叫字段标识');
            $table->text('config')->comment('模块配置项');
            $table->mediumText('permission')->nullable()->comment('权限配置，目的就是为了在权限列表显示时连同模块名称一同显示');
            $table->text('admin_menu')->nullable()->comment('后台菜单');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_modules');
    }
}
