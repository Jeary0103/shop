<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_bianhao')->default('')->comment("订单编号");
            $table->enum('status',['未付款','已付款','已发货','已完成'])->default('未付款')->comment('订单状态');
            $table->unsignedInteger('address_id')->default(0)->comment('地址');
            $table->unsignedInteger('user_id')->default(0)->comment('用户编号');
            $table->unsignedInteger('total_price')->default(0)->comment('总价');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
