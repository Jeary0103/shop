<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'order__details',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('order_id')->default(0)->comment('订单编号');
                // 这里使用了外键约束，当删除了商品信息时，对应的货品信息一同被删除，很好玩！！
                // 属于数据迁移部分的知识
                $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
                $table->unsignedInteger('goods_id')->default(0)->comment('商品编号');
                $table->unsignedInteger('product_id')->default(0)->comment('货品编号');
                $table->unsignedInteger('num')->default(0)->comment('购买数量');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order__details');
    }
}
