<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('收件人姓名');
            $table->string('phone')->comment('收件人电话');
            $table->string('area')->comment('所在城市');
            $table->string('address')->comment('详细地址');
            $table->tinyInteger('is_default')->default(0)->comment('设置为默认地址');
            $table->unsignedInteger('user_id')->comment('用户编号');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
