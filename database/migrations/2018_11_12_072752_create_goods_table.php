<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('名称');
            $table->integer('price')->comment('价格');
            $table->string('category_id')->comment('分类');
            $table->text('pics')->comment('图片');
            $table->text('description')->comment('描述');
            $table->mediumText('content')->comment('详情');
            $table->tinyInteger('is_commend')->default(0)->comment('是否推荐');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods');
    }
}
