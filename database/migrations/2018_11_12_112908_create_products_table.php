<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attrs')->comment('货品属性');
            $table->smallInteger('kucun')->comment('货品库存量');
            $table->smallInteger('add_price')->comment('货品附加价格');
            $table->unsignedInteger('goods_id')->comment('商品id');
            // 这里使用了外键约束，当删除了商品信息时，对应的货品信息一同被删除，很好玩！！
            // 属于数据迁移部分的知识
            $table->foreign('goods_id')->references('id')->on('goods')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
