<?php
//Route::get('/', function () {
//    return view('welcome');
//});
// 前台首页路由
Route::group(['namespace'=>'Home','as'=>'home.'],function (){
    // 前台首页路由
    Route::get('/','HomeController@index')->name('index');
    // 详情页路由
    Route::get('/content/{good}/{product?}','HomeController@content')->name('content');
    // 列表页路由
    Route::get('/lists/{category}','HomeController@lists')->name('lists');

    // 前台登录、退出、注册路由
    Route::get('/login','LoginController@loginForm')->name('login');
    Route::post('/login','LoginController@login')->name('login');
    Route::get('/logout','LoginController@logout')->name('logout');
    Route::get('/regist','RegistController@regist')->name('regist');
    Route::post('/regist','RegistController@store')->name('regist');
    // 微信支付异步通知路由,异步是微信的请求，对于微信而言，不存在什么令牌一说，所以要想请求到laravel里面的数据，
    // 就应该把微信请求的路由规则加入白名单，即 中间件里面的 verifycsrftoken文件设置就可以了。
    Route::post('wxPayResult','PayController@wxPayResult')->name('wxPayResult');
});
/**
 *  需要验证的路由
 */
Route::group(['middleware'=>'auth.login','namespace'=>'Home','as'=>'home.'],function (){
    // 加入购物车路由
    Route::get('/addCart/{good}/{product}/{num}','CartController@addCart')->name('addCart');
    // 立即购买
    Route::post('quickBuy','CartController@quickBuy')->name('quickBuy');
    // 我的购物车
    Route::get('/myCart','CartController@myCart')->name('myCart');
    // 删除购物车数据路由
    Route::get('/deleteCart/{cart}','CartController@delete')->name('myCart.delete');
    // 购物车数量加1
    Route::get('/plus/{cart}','CartController@plus')->name('myCart.plus');
    // 购物车数量减1
    Route::get('/minus/{cart}','CartController@minus')->name('myCart.minus');
    // 个人中心路由
    Route::get('/center/{bianliang}','CenterController@index')->name('center');
    // 个人中心修改密码路由
    Route::put('/center/changePassword/','UserController@changPassword')->name('changePassword');
    // 个人中心修改个人信息的路由
    Route::put('/center/gerenXinxi','UserController@gerenXinxi')->name('gerenXinxi');
    // 个人中心收货人地址资源路由
    Route::resource('/center/address','AddressController');
    // 设置默认地址路由
    Route::get('/center/setDefault/{address}','AddressController@setDefault')->name('setDefault');
    // 去结算的订单列表页面
    Route::get('/order_list','OrderController@orders')->name('order_list');
    // 订单的资源路由
    Route::resource('/order','OrderController');
    // 异步删除我的订单路由
    Route::get('/delOrder/{order_id}','CenterController@delOrder')->name('delOrder');
    // 提交订单成功以后，跳转的支付订单路由
    Route::get('/pay/{order_bianhao}','PayController@index')->name('pay');
    //检测订单支付状态
    Route::post('checkOrderStatus','PayController@checkOrderStatus')->name('checkOrderStatus');
});
/*
 *【后台登录验证的路由组】
 */
Route::group(['middleware'=>'admin.auth','prefix'=>'admin','namespace'=>'Admin','as'=>'admin.'],function (){
    Route::get('/','AdminController@index')->name('index');
    // 分类管理资源路由
    Route::resource('/category','CategoryController');
    // 获取子分类的路由
    Route::get('/getSonCategory/{id}','CategoryController@getSonCategory')->name('getSonCategory');
    // 商品管理资源路由
    Route::resource('/goods','GoodsController');
    // 后台，订单管理路由
    Route::resource('order','OrderController');
    // 幻灯片管理
    Route::resource('slide','SlideController');
    // 权限列表路由
    Route::get('admin/permission','PermissionController@index')->name('permission');
    // 角色资源路由
    Route::resource('admin/role','RoleController');

});

/*
 * 【后台登录不需要验证的路由组】
 */
Route::group(['prefix'=>'admin','namespace'=>'Admin','as'=>'admin.'],function (){
    Route::get('/login','LoginController@loginForm')->name('login');
    Route::post('/login','LoginController@login')->name('login');
    Route::get('/logout','LoginController@logout')->name('logout');
});

/**
 * 公共控制器  图片上传类
 */
Route::group(
    ['prefix' => 'common', 'namespace' => 'Common', 'as' => 'common.'],
    function () {
        Route::any('uploader/make', 'UploaderController@make')->name('uploader.make');
        Route::any('uploader/lists', 'UploaderController@lists')->name('uploader.lists');
        // layui图片上传路由
        Route::any('uploader/layuiUpload','UploaderController@layuiUpload')->name('uploader.layuiUpload');
        // 发送验证码类
        Route::any('code/send','CodeController@send')->name('code.send');
    }
);

